var namespace_repository =
[
    [ "Character", "class_repository_1_1_character.html", "class_repository_1_1_character" ],
    [ "GameModel", "class_repository_1_1_game_model.html", "class_repository_1_1_game_model" ],
    [ "Highscore", "class_repository_1_1_highscore.html", "class_repository_1_1_highscore" ],
    [ "HighscoreRepository", "class_repository_1_1_highscore_repository.html", "class_repository_1_1_highscore_repository" ],
    [ "ICharacter", "interface_repository_1_1_i_character.html", "interface_repository_1_1_i_character" ],
    [ "IGameModel", "interface_repository_1_1_i_game_model.html", "interface_repository_1_1_i_game_model" ],
    [ "IHighscoreRepository", "interface_repository_1_1_i_highscore_repository.html", "interface_repository_1_1_i_highscore_repository" ]
];