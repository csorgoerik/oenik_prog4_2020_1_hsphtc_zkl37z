var interface_repository_1_1_i_game_model =
[
    [ "AddPoints", "interface_repository_1_1_i_game_model.html#a1e316b302074756d672fddf432ff4313", null ],
    [ "ChangeTarget", "interface_repository_1_1_i_game_model.html#aea3d3853dbea7134aaef096e11abee5a", null ],
    [ "DamageEnemy", "interface_repository_1_1_i_game_model.html#a507e840631b4a4d44e85621e9ea671c6", null ],
    [ "DamageHero", "interface_repository_1_1_i_game_model.html#a88da9c489c312aa09bed15bf8a96eb9d", null ],
    [ "NextRound", "interface_repository_1_1_i_game_model.html#ae3bdf1933969bc1b36816c46e0a6d185", null ],
    [ "Save", "interface_repository_1_1_i_game_model.html#a9fb012b0702278846c006f1f0a6b023a", null ],
    [ "BossEnemies", "interface_repository_1_1_i_game_model.html#a8c2235979e353bfa02f02726b93bea30", null ],
    [ "CommonEnemies", "interface_repository_1_1_i_game_model.html#ab754e80a110979a2140ef1762974c1c7", null ],
    [ "CurrentEnemies", "interface_repository_1_1_i_game_model.html#a9261a3737a75bec8b4c8ebfeeffd1d7a", null ],
    [ "CurrentHeroes", "interface_repository_1_1_i_game_model.html#a7d67d502551f1521f9520057d5dada90", null ],
    [ "FirstSelected", "interface_repository_1_1_i_game_model.html#a25c51ee346f103fa2fa608121908b400", null ],
    [ "GameField", "interface_repository_1_1_i_game_model.html#a6952b243b8b77641e296a8ff7e4b1b8b", null ],
    [ "GameHeight", "interface_repository_1_1_i_game_model.html#aff938ead9a67f67700a2b00e22a4824d", null ],
    [ "GameWidth", "interface_repository_1_1_i_game_model.html#a2377e2f0a5027ebf76ee3fcf95f15458", null ],
    [ "Heroes", "interface_repository_1_1_i_game_model.html#a1a7b0e4852881c299b28472dcdaf4c71", null ],
    [ "IsFinished", "interface_repository_1_1_i_game_model.html#ae45c491bd11902a384784c0c0b6b8f79", null ],
    [ "Points", "interface_repository_1_1_i_game_model.html#a4e73bb867568ba7b3f7218468c7f6f51", null ],
    [ "SecondSelected", "interface_repository_1_1_i_game_model.html#ad6151cfc3de65c1d8c49dbeb72b42281", null ],
    [ "Speed", "interface_repository_1_1_i_game_model.html#a35e36d2dcdaab2ff3ba69bcc358c829e", null ],
    [ "Target", "interface_repository_1_1_i_game_model.html#a593dd22d216d5c386aa01fd3e25ae362", null ],
    [ "TileSize", "interface_repository_1_1_i_game_model.html#aecf47307167e8e65d9f8f8acbc240d78", null ]
];