var interface_repository_1_1_i_character =
[
    [ "CurrentHp", "interface_repository_1_1_i_character.html#a7feba464bd5400b176cefd2d7b296026", null ],
    [ "Damage", "interface_repository_1_1_i_character.html#a0517b6edd05e828c3ada842b7cd5679f", null ],
    [ "Image", "interface_repository_1_1_i_character.html#a9dd82dac6276830eac715417e6a938e2", null ],
    [ "IsAlive", "interface_repository_1_1_i_character.html#a267fb5fb6e054bdad09ccf191ee54d2f", null ],
    [ "IsUsable", "interface_repository_1_1_i_character.html#a7a36ec70894125d66f1881af88483d24", null ],
    [ "MaxHp", "interface_repository_1_1_i_character.html#adf6b7ffc8d96c3651cd69b64e5ab4c81", null ],
    [ "Name", "interface_repository_1_1_i_character.html#a83a3a97618a9cd0b2d9658c8ea16036b", null ],
    [ "Speed", "interface_repository_1_1_i_character.html#a6c0ac0c17ff1f5751196ee2e1b3971b7", null ],
    [ "Target", "interface_repository_1_1_i_character.html#a4fc9db60f7bb09eed486f508e9c999d5", null ],
    [ "Type", "interface_repository_1_1_i_character.html#a3334cb11f3a909d126087a9e197e62f1", null ]
];