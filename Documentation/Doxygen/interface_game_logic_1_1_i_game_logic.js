var interface_game_logic_1_1_i_game_logic =
[
    [ "BossTick", "interface_game_logic_1_1_i_game_logic.html#a5a05de3ce61bc70ff220abdfe7480a86", null ],
    [ "CheckRowAndColumn", "interface_game_logic_1_1_i_game_logic.html#a781afad7ce04632a06d7ac392563f45c", null ],
    [ "ClickScreen", "interface_game_logic_1_1_i_game_logic.html#ab053a0bce7bfa0d9fd0a6efdafb8b6fc", null ],
    [ "HeroAction", "interface_game_logic_1_1_i_game_logic.html#ad9f60f071a5677eb4b1d8f707a756764", null ],
    [ "HeroTick", "interface_game_logic_1_1_i_game_logic.html#a4b8a73af1bebce77b65870ce0414bdf0", null ],
    [ "LeftEnemyTick", "interface_game_logic_1_1_i_game_logic.html#abb2e7f4a8137951eb2f518b0e12fa535", null ],
    [ "RightEnemyTick", "interface_game_logic_1_1_i_game_logic.html#a57d1439329546e8e236c29826e508065", null ],
    [ "SaveScore", "interface_game_logic_1_1_i_game_logic.html#adae6a248a28c4f9d612a78cee1838904", null ],
    [ "SelectCrystal", "interface_game_logic_1_1_i_game_logic.html#ac79c775474bbace5ea2daf7b213d991e", null ],
    [ "CurrentGame", "interface_game_logic_1_1_i_game_logic.html#a0474d98b34c13f93e7228c94205110ba", null ]
];