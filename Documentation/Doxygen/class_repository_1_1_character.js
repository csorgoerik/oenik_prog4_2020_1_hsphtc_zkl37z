var class_repository_1_1_character =
[
    [ "Character", "class_repository_1_1_character.html#a5d284962d16468b37fcfbfa96a00b32e", null ],
    [ "CurrentHp", "class_repository_1_1_character.html#ae6a0e8003c6b5d8872eee6cdba085f48", null ],
    [ "Damage", "class_repository_1_1_character.html#a28d9e19b36d3f8a77d84e82926b48ffd", null ],
    [ "Image", "class_repository_1_1_character.html#acf3017eddbeeb3ecaf5972202520b14c", null ],
    [ "IsAlive", "class_repository_1_1_character.html#afea1a8c736cb314280355a977aded976", null ],
    [ "IsUsable", "class_repository_1_1_character.html#a0f8024be4bd95bcb26270ad56a76a0c1", null ],
    [ "MaxHp", "class_repository_1_1_character.html#ac3f76d140bf8e119257720d7493cdc5f", null ],
    [ "Name", "class_repository_1_1_character.html#a89943b58c34fbe3d40556eb401d6e467", null ],
    [ "Speed", "class_repository_1_1_character.html#a39ff5b0ecc7ead1cc4d508fa60858352", null ],
    [ "Target", "class_repository_1_1_character.html#af6f88618333a607bb05c922d3c0f3588", null ],
    [ "Type", "class_repository_1_1_character.html#a38d49c0aa427589de1503b76b96e5977", null ]
];