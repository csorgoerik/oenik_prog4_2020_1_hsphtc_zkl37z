var class_game_logic_1_1_game_logic =
[
    [ "GameLogic", "class_game_logic_1_1_game_logic.html#a2cffe4f15e2191778ab86174ade4be5e", null ],
    [ "BossTick", "class_game_logic_1_1_game_logic.html#a84f5afb904b969e27192c18d4037674f", null ],
    [ "CheckRowAndColumn", "class_game_logic_1_1_game_logic.html#aab562313fb897a970cd991e0e56d1479", null ],
    [ "ClickScreen", "class_game_logic_1_1_game_logic.html#ac63d3d716d8fae10b1fbc275e44885ed", null ],
    [ "DamageEnemy", "class_game_logic_1_1_game_logic.html#a38c2950db61664039e7901bac0496e09", null ],
    [ "DamageHero", "class_game_logic_1_1_game_logic.html#a5f34b62eb1502bcfa41a13f845e0390e", null ],
    [ "EnemyAction", "class_game_logic_1_1_game_logic.html#a9963cbbdc1442addebb79bdc8fc920d2", null ],
    [ "HeroAction", "class_game_logic_1_1_game_logic.html#a28d499591cc4fd588a48c40256ccd54e", null ],
    [ "HeroTick", "class_game_logic_1_1_game_logic.html#afe23470d8c4a6a77f7239e9e0f43eb25", null ],
    [ "LeftEnemyTick", "class_game_logic_1_1_game_logic.html#a5bac362bdf92741780bf9e2c1df69977", null ],
    [ "RightEnemyTick", "class_game_logic_1_1_game_logic.html#a98a338d7be28054d08fd27bbb243b0c5", null ],
    [ "Save", "class_game_logic_1_1_game_logic.html#afed32513e8a6b0762b3b4bf7b880651e", null ],
    [ "SaveScore", "class_game_logic_1_1_game_logic.html#a0f7b4895c4a62cd2f56f7b3c19243bdc", null ],
    [ "SelectCrystal", "class_game_logic_1_1_game_logic.html#a8b702b36d58691580af1eff7d89ab550", null ],
    [ "CurrentGame", "class_game_logic_1_1_game_logic.html#aeb78aa3ab0707304d97a49c316f202c5", null ],
    [ "HighscoreRepo", "class_game_logic_1_1_game_logic.html#ab4e579ec9393a4573ecf7da83a738c31", null ]
];