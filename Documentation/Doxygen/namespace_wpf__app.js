var namespace_wpf__app =
[
    [ "Properties", "namespace_wpf__app_1_1_properties.html", "namespace_wpf__app_1_1_properties" ],
    [ "App", "class_wpf__app_1_1_app.html", "class_wpf__app_1_1_app" ],
    [ "EndGamePage", "class_wpf__app_1_1_end_game_page.html", "class_wpf__app_1_1_end_game_page" ],
    [ "GameControl", "class_wpf__app_1_1_game_control.html", "class_wpf__app_1_1_game_control" ],
    [ "GamePage", "class_wpf__app_1_1_game_page.html", "class_wpf__app_1_1_game_page" ],
    [ "GameRenderer", "class_wpf__app_1_1_game_renderer.html", "class_wpf__app_1_1_game_renderer" ],
    [ "HeroSelectionPage", "class_wpf__app_1_1_hero_selection_page.html", "class_wpf__app_1_1_hero_selection_page" ],
    [ "MainMenu", "class_wpf__app_1_1_main_menu.html", "class_wpf__app_1_1_main_menu" ],
    [ "MainWindow", "class_wpf__app_1_1_main_window.html", "class_wpf__app_1_1_main_window" ],
    [ "Page1", "class_wpf__app_1_1_page1.html", "class_wpf__app_1_1_page1" ],
    [ "ScoreBoard", "class_wpf__app_1_1_score_board.html", "class_wpf__app_1_1_score_board" ],
    [ "Window1", "class_wpf__app_1_1_window1.html", "class_wpf__app_1_1_window1" ]
];