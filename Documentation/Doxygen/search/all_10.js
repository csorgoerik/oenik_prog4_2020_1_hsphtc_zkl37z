var searchData=
[
  ['tank_99',['Tank',['../namespace_repository.html#a8c6d8a5b4d9dc03a50b26f8244426b1fae4419464d570c4b497ba1979e790b104',1,'Repository']]],
  ['target_100',['Target',['../class_repository_1_1_character.html#af6f88618333a607bb05c922d3c0f3588',1,'Repository.Character.Target()'],['../class_repository_1_1_game_model.html#a980b43419a840dcf785eef7e750f9667',1,'Repository.GameModel.Target()'],['../interface_repository_1_1_i_character.html#a4fc9db60f7bb09eed486f508e9c999d5',1,'Repository.ICharacter.Target()'],['../interface_repository_1_1_i_game_model.html#a593dd22d216d5c386aa01fd3e25ae362',1,'Repository.IGameModel.Target()']]],
  ['test_101',['Test',['../namespace_test.html',1,'']]],
  ['test_5fchangetarget_102',['Test_ChangeTarget',['../class_test_1_1_logic_test.html#a16c33395d11ddc4d3e9af0b25ae1e577',1,'Test::LogicTest']]],
  ['test_5fcheckrowandcolumn1_103',['Test_CheckRowAndColumn1',['../class_test_1_1_logic_test.html#aa98217efd39d5a16763e9e871898497d',1,'Test::LogicTest']]],
  ['test_5fheroactionkill_104',['Test_HeroActionKill',['../class_test_1_1_logic_test.html#a0dc79d4b9169600c85df7b88b8cacd13',1,'Test::LogicTest']]],
  ['test_5fnextround_105',['Test_NextRound',['../class_test_1_1_logic_test.html#a051985d1450f387a43c1606e53100a5e',1,'Test::LogicTest']]],
  ['test_5ftankaction_106',['Test_TankAction',['../class_test_1_1_logic_test.html#a4e42382c3ec689bbd7f5c19a4aaba792',1,'Test::LogicTest']]],
  ['tilesize_107',['TileSize',['../class_repository_1_1_game_model.html#a80bb03104e96f573f3762bb9e6b97694',1,'Repository.GameModel.TileSize()'],['../interface_repository_1_1_i_game_model.html#aecf47307167e8e65d9f8f8acbc240d78',1,'Repository.IGameModel.TileSize()']]],
  ['type_108',['Type',['../class_repository_1_1_character.html#a38d49c0aa427589de1503b76b96e5977',1,'Repository.Character.Type()'],['../interface_repository_1_1_i_character.html#a3334cb11f3a909d126087a9e197e62f1',1,'Repository.ICharacter.Type()']]]
];
