var searchData=
[
  ['image_226',['Image',['../class_repository_1_1_character.html#acf3017eddbeeb3ecaf5972202520b14c',1,'Repository.Character.Image()'],['../interface_repository_1_1_i_character.html#a9dd82dac6276830eac715417e6a938e2',1,'Repository.ICharacter.Image()']]],
  ['isalive_227',['IsAlive',['../class_repository_1_1_character.html#afea1a8c736cb314280355a977aded976',1,'Repository.Character.IsAlive()'],['../interface_repository_1_1_i_character.html#a267fb5fb6e054bdad09ccf191ee54d2f',1,'Repository.ICharacter.IsAlive()']]],
  ['isfinished_228',['IsFinished',['../class_repository_1_1_game_model.html#aa1db42a3ce0316e7ac6509e4db432baf',1,'Repository.GameModel.IsFinished()'],['../interface_repository_1_1_i_game_model.html#ae45c491bd11902a384784c0c0b6b8f79',1,'Repository.IGameModel.IsFinished()']]],
  ['isusable_229',['IsUsable',['../class_repository_1_1_character.html#a0f8024be4bd95bcb26270ad56a76a0c1',1,'Repository.Character.IsUsable()'],['../interface_repository_1_1_i_character.html#a7a36ec70894125d66f1881af88483d24',1,'Repository.ICharacter.IsUsable()']]]
];
