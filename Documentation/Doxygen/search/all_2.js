var searchData=
[
  ['changetarget_8',['ChangeTarget',['../class_repository_1_1_game_model.html#a1d639477ea5b0262f39eaa170fbe3b7d',1,'Repository.GameModel.ChangeTarget()'],['../interface_repository_1_1_i_game_model.html#aea3d3853dbea7134aaef096e11abee5a',1,'Repository.IGameModel.ChangeTarget()']]],
  ['character_9',['Character',['../class_repository_1_1_character.html',1,'Repository.Character'],['../class_repository_1_1_character.html#a5d284962d16468b37fcfbfa96a00b32e',1,'Repository.Character.Character()']]],
  ['charactertype_10',['CharacterType',['../namespace_repository.html#a8c6d8a5b4d9dc03a50b26f8244426b1f',1,'Repository']]],
  ['checkrowandcolumn_11',['CheckRowAndColumn',['../class_game_logic_1_1_game_logic.html#aab562313fb897a970cd991e0e56d1479',1,'GameLogic.GameLogic.CheckRowAndColumn()'],['../interface_game_logic_1_1_i_game_logic.html#a781afad7ce04632a06d7ac392563f45c',1,'GameLogic.IGameLogic.CheckRowAndColumn()']]],
  ['clickscreen_12',['ClickScreen',['../class_game_logic_1_1_game_logic.html#ac63d3d716d8fae10b1fbc275e44885ed',1,'GameLogic.GameLogic.ClickScreen()'],['../interface_game_logic_1_1_i_game_logic.html#ab053a0bce7bfa0d9fd0a6efdafb8b6fc',1,'GameLogic.IGameLogic.ClickScreen()']]],
  ['commonenemies_13',['CommonEnemies',['../class_repository_1_1_game_model.html#a8c2c094b4fc4da5e06b402fcc79e1e12',1,'Repository.GameModel.CommonEnemies()'],['../interface_repository_1_1_i_game_model.html#ab754e80a110979a2140ef1762974c1c7',1,'Repository.IGameModel.CommonEnemies()']]],
  ['convertfieldtostring_14',['ConvertFieldToString',['../class_repository_1_1_game_model.html#a186e84f1cc2a254efe218cbf8ffefece',1,'Repository::GameModel']]],
  ['convertstringtofield_15',['ConvertStringToField',['../class_repository_1_1_game_model.html#a8d962449aa4bddae2381e3bdff12713e',1,'Repository::GameModel']]],
  ['createdelegate_16',['CreateDelegate',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['createinstance_17',['CreateInstance',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['criticalstrike_18',['CriticalStrike',['../namespace_repository.html#a790f2278fd62cf8b7d6495df58d52543afb6b348569bee4aabee3e198885a5c2b',1,'Repository']]],
  ['crystaltype_19',['CrystalType',['../namespace_repository.html#a790f2278fd62cf8b7d6495df58d52543',1,'Repository']]],
  ['culture_20',['Culture',['../class_wpf__app_1_1_properties_1_1_resources.html#a2e2a4537eab22c2c82861438e64b0f5a',1,'Wpf_app::Properties::Resources']]],
  ['currentenemies_21',['CurrentEnemies',['../class_repository_1_1_game_model.html#a3b537ebc5348d426b7abfbe8c1b08952',1,'Repository.GameModel.CurrentEnemies()'],['../interface_repository_1_1_i_game_model.html#a9261a3737a75bec8b4c8ebfeeffd1d7a',1,'Repository.IGameModel.CurrentEnemies()']]],
  ['currentgame_22',['CurrentGame',['../class_game_logic_1_1_game_logic.html#aeb78aa3ab0707304d97a49c316f202c5',1,'GameLogic.GameLogic.CurrentGame()'],['../interface_game_logic_1_1_i_game_logic.html#a0474d98b34c13f93e7228c94205110ba',1,'GameLogic.IGameLogic.CurrentGame()']]],
  ['currentheroes_23',['CurrentHeroes',['../class_repository_1_1_game_model.html#aeb8a2dd4dc858f615e3dac76f6e1e8fb',1,'Repository.GameModel.CurrentHeroes()'],['../interface_repository_1_1_i_game_model.html#a7d67d502551f1521f9520057d5dada90',1,'Repository.IGameModel.CurrentHeroes()']]],
  ['currenthp_24',['CurrentHp',['../class_repository_1_1_character.html#ae6a0e8003c6b5d8872eee6cdba085f48',1,'Repository.Character.CurrentHp()'],['../interface_repository_1_1_i_character.html#a7feba464bd5400b176cefd2d7b296026',1,'Repository.ICharacter.CurrentHp()']]],
  ['castle_20core_20changelog_25',['Castle Core Changelog',['../md__f_1__egyetem_prog4_f_xC3_xA9l_xC3_xA9ves__f_xC3_xA9l_xC3_xA9ves__o_e_n_i_k__p_r_o_g4_2020_1_9cdcaa0ce005797acf55df7ef4ae81e9.html',1,'']]]
];
