var searchData=
[
  ['blue_4',['Blue',['../namespace_repository.html#a790f2278fd62cf8b7d6495df58d52543a9594eec95be70e7b1710f730fdda33d9',1,'Repository']]],
  ['bossenemies_5',['BossEnemies',['../class_repository_1_1_game_model.html#a8a90064dbde18e6b22141424479a3b42',1,'Repository.GameModel.BossEnemies()'],['../interface_repository_1_1_i_game_model.html#a8c2235979e353bfa02f02726b93bea30',1,'Repository.IGameModel.BossEnemies()']]],
  ['bosstick_6',['BossTick',['../class_game_logic_1_1_game_logic.html#a84f5afb904b969e27192c18d4037674f',1,'GameLogic.GameLogic.BossTick()'],['../interface_game_logic_1_1_i_game_logic.html#a5a05de3ce61bc70ff220abdfe7480a86',1,'GameLogic.IGameLogic.BossTick()']]],
  ['builddrawing_7',['BuildDrawing',['../class_wpf__app_1_1_game_renderer.html#a5c76d74f73f2f1ac4d041738d4771e11',1,'Wpf_app::GameRenderer']]]
];
