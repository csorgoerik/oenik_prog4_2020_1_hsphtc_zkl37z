var searchData=
[
  ['save_184',['Save',['../class_game_logic_1_1_game_logic.html#afed32513e8a6b0762b3b4bf7b880651e',1,'GameLogic.GameLogic.Save()'],['../class_repository_1_1_game_model.html#a60f286e5d55cfde0e1ba22db8845b2d0',1,'Repository.GameModel.Save()'],['../interface_repository_1_1_i_game_model.html#a9fb012b0702278846c006f1f0a6b023a',1,'Repository.IGameModel.Save()']]],
  ['savehighscore_185',['SaveHighscore',['../class_repository_1_1_highscore_repository.html#a084ad826a65b626ebe70594b8bbfca95',1,'Repository.HighscoreRepository.SaveHighscore()'],['../interface_repository_1_1_i_highscore_repository.html#aaf74b7f515a552f0bfda462c5ffca4c5',1,'Repository.IHighscoreRepository.SaveHighscore()']]],
  ['savescore_186',['SaveScore',['../class_game_logic_1_1_game_logic.html#a0f7b4895c4a62cd2f56f7b3c19243bdc',1,'GameLogic.GameLogic.SaveScore()'],['../interface_game_logic_1_1_i_game_logic.html#adae6a248a28c4f9d612a78cee1838904',1,'GameLogic.IGameLogic.SaveScore()']]],
  ['scoreboard_187',['ScoreBoard',['../class_wpf__app_1_1_score_board.html#a19d4dc91a56a54d8dcef6beccb9f6dad',1,'Wpf_app::ScoreBoard']]],
  ['selectcrystal_188',['SelectCrystal',['../class_game_logic_1_1_game_logic.html#a8b702b36d58691580af1eff7d89ab550',1,'GameLogic.GameLogic.SelectCrystal()'],['../interface_game_logic_1_1_i_game_logic.html#ac79c775474bbace5ea2daf7b213d991e',1,'GameLogic.IGameLogic.SelectCrystal()']]],
  ['setpropertyvalue_189',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]]
];
