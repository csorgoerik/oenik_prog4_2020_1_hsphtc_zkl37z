var hierarchy =
[
    [ "Application", null, [
      [ "Wpf_app.App", "class_wpf__app_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "Wpf_app.Properties.Settings", "class_wpf__app_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "Wpf_app.GameControl", "class_wpf__app_1_1_game_control.html", null ]
    ] ],
    [ "Wpf_app.GameRenderer", "class_wpf__app_1_1_game_renderer.html", null ],
    [ "Repository.Highscore", "class_repository_1_1_highscore.html", null ],
    [ "Repository.ICharacter", "interface_repository_1_1_i_character.html", [
      [ "Repository.Character", "class_repository_1_1_character.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "Wpf_app.Page1", "class_wpf__app_1_1_page1.html", null ],
      [ "Wpf_app.ScoreBoard", "class_wpf__app_1_1_score_board.html", null ],
      [ "Wpf_app.Window1", "class_wpf__app_1_1_window1.html", null ]
    ] ],
    [ "GameLogic.IGameLogic", "interface_game_logic_1_1_i_game_logic.html", [
      [ "GameLogic.GameLogic", "class_game_logic_1_1_game_logic.html", null ]
    ] ],
    [ "Repository.IGameModel", "interface_repository_1_1_i_game_model.html", [
      [ "Repository.GameModel", "class_repository_1_1_game_model.html", null ]
    ] ],
    [ "Repository.IHighscoreRepository", "interface_repository_1_1_i_highscore_repository.html", [
      [ "Repository.HighscoreRepository", "class_repository_1_1_highscore_repository.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Test.LogicTest", "class_test_1_1_logic_test.html", null ],
    [ "Page", null, [
      [ "Wpf_app.Page1", "class_wpf__app_1_1_page1.html", null ],
      [ "Wpf_app.ScoreBoard", "class_wpf__app_1_1_score_board.html", null ]
    ] ],
    [ "Page", null, [
      [ "Wpf_app.EndGamePage", "class_wpf__app_1_1_end_game_page.html", null ],
      [ "Wpf_app.GamePage", "class_wpf__app_1_1_game_page.html", null ],
      [ "Wpf_app.HeroSelectionPage", "class_wpf__app_1_1_hero_selection_page.html", null ],
      [ "Wpf_app.MainMenu", "class_wpf__app_1_1_main_menu.html", null ]
    ] ],
    [ "Wpf_app.Properties.Resources", "class_wpf__app_1_1_properties_1_1_resources.html", null ],
    [ "Test.UnitTest1", "class_test_1_1_unit_test1.html", null ],
    [ "Window", null, [
      [ "Wpf_app.MainWindow", "class_wpf__app_1_1_main_window.html", null ],
      [ "Wpf_app.Window1", "class_wpf__app_1_1_window1.html", null ]
    ] ]
];