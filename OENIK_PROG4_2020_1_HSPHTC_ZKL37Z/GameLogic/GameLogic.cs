﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using Repository;

    /// <summary>
    /// Game logic class.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private static Random rnd = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// Constructor for GameLogic.
        /// </summary>
        /// <param name="currentGame">Current game model.</param>
        public GameLogic(IGameModel currentGame)
        {
            this.CurrentGame = currentGame;
            this.HighscoreRepo = new HighscoreRepository();
        }

        /// <summary>
        /// Gets or sets game model.
        /// </summary>
        public virtual IGameModel CurrentGame { get; set; }

        /// <summary>
        /// Gets or sets highscore repository.
        /// </summary>
        public IHighscoreRepository HighscoreRepo { get; set; }

        /// <summary>
        /// Checks if there is any activable crystal.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="type">Crystal type.</param>
        /// <returns>bool.</returns>
        public bool CheckRowAndColumn(int x, int y, CrystalType type)
        {
            int left = this.GetLeftIndex(x, y, type);
            int right = this.GetRightIndex(x, y, type);
            int top = this.GetTopIndex(x, y, type);
            int bottom = this.GetBottomIndex(x, y, type);
            int horizontalDif = Math.Abs(right - left) + 1;
            int verticalDif = Math.Abs(bottom - top) + 1;

            if (horizontalDif > 3 || verticalDif > 3)
            {
                int dif = horizontalDif < verticalDif ? verticalDif : horizontalDif;
                bool dir = horizontalDif < verticalDif;
                this.Fill(left, right, top, bottom, x, y, dir);
                switch (dif)
                {
                    case 4:
                        this.CurrentGame.AddPoints(50);
                        this.DamageEnemy(10 * dif);
                        break;
                    case 5:
                        this.CurrentGame.AddPoints(60);
                        this.DamageEnemy(10 * dif);
                        break;
                    case 6:
                        this.CurrentGame.AddPoints(80);
                        this.DamageEnemy(10 * dif);
                        break;
                    case 7:
                        this.CurrentGame.AddPoints(110);
                        this.DamageEnemy(10 * dif);
                        break;
                    default:
                        break;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Execute heroes special action.
        /// </summary>
        /// <param name="id">hero.</param>
        public void HeroAction(int id)
        {
            if (this.CurrentGame.CurrentHeroes[id].IsUsable)
            {
                switch (this.CurrentGame.CurrentHeroes[id].Type)
                {
                    case CharacterType.Archer:
                        this.DamageEnemy(this.CurrentGame.CurrentHeroes[id].Damage);
                        break;
                    case CharacterType.Healer:
                        this.Heal(ref this.CurrentGame.CurrentHeroes[0]);
                        this.Heal(ref this.CurrentGame.CurrentHeroes[1]);
                        this.Heal(ref this.CurrentGame.CurrentHeroes[2]);
                        break;
                    case CharacterType.Fighter:
                        this.DamageEnemy(this.CurrentGame.CurrentHeroes[id].Damage);
                        break;
                    case CharacterType.Mage:
                        this.MageAction(true);
                        break;
                    case CharacterType.Tank:
                        this.TankAction(id, true);
                        break;
                    default:
                        break;
                }

                this.CurrentGame.CurrentHeroes[id].IsUsable = false;
            }
        }

        /// <summary>
        /// Selects one crystal.
        /// </summary>
        /// <param name="x">Crystal x coordinates.</param>
        /// <param name="y">Crystal Y coordinates.</param>
        public void SelectCrystal(int x, int y)
        {
            this.NewTarget(true);
            Point newPoint = new Point(x, y);
            if (this.CurrentGame.FirstSelected != newPoint && this.CurrentGame.SecondSelected != newPoint)
            {
                if (this.CurrentGame.FirstSelected.X == -1 && this.CurrentGame.FirstSelected.Y == -1)
                {
                    this.CurrentGame.FirstSelected = newPoint;
                    switch (this.CurrentGame.GameField[(int)this.CurrentGame.FirstSelected.Y, (int)this.CurrentGame.FirstSelected.X])
                    {
                        case CrystalType.Execution:
                            this.Execution(this.CurrentGame.FirstSelected);
                            this.CurrentGame.FirstSelected = new Point(-1, -1);
                            break;
                        case CrystalType.DFA:
                            this.DeathFromAbove(this.CurrentGame.FirstSelected);
                            this.CurrentGame.FirstSelected = new Point(-1, -1);
                            break;
                        case CrystalType.CriticalStrike:
                            this.CriticalStrike(this.CurrentGame.FirstSelected);
                            this.CurrentGame.FirstSelected = new Point(-1, -1);
                            break;
                        case CrystalType.Shapeshifter:
                            this.Shapeshifter(this.CurrentGame.FirstSelected);
                            this.CurrentGame.FirstSelected = new Point(-1, -1);
                            break;
                        default:
                            if (this.CurrentGame.SecondSelected.X != -1 && this.CurrentGame.SecondSelected.Y != -1)
                            {
                                this.Swap();
                            }

                            break;
                    }
                }
                else if (this.CurrentGame.SecondSelected.X == -1 && this.CurrentGame.SecondSelected.Y == -1)
                {
                    this.CurrentGame.SecondSelected = newPoint;
                    switch (this.CurrentGame.GameField[(int)this.CurrentGame.SecondSelected.Y, (int)this.CurrentGame.SecondSelected.X])
                    {
                        case CrystalType.Execution:
                            this.Execution(this.CurrentGame.SecondSelected);
                            this.CurrentGame.SecondSelected = new Point(-1, -1);
                            break;
                        case CrystalType.DFA:
                            this.DeathFromAbove(this.CurrentGame.SecondSelected);
                            this.CurrentGame.SecondSelected = new Point(-1, -1);
                            break;
                        case CrystalType.CriticalStrike:
                            this.CriticalStrike(this.CurrentGame.SecondSelected);
                            this.CurrentGame.SecondSelected = new Point(-1, -1);
                            break;
                        case CrystalType.Shapeshifter:
                            this.Shapeshifter(this.CurrentGame.SecondSelected);
                            this.CurrentGame.SecondSelected = new Point(-1, -1);
                            break;
                        default:
                            this.Swap();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Detemines which action should be activated by click position.
        /// </summary>
        /// <param name="p">Click coordinates.</param>
        /// <returns>string.</returns>
        public string ClickScreen(Point p)
        {
            string action = string.Empty;

            if (p.X > this.CurrentGame.GameWidth - this.CurrentGame.TileSize && p.Y < this.CurrentGame.TileSize)
            {
                this.Save();
                action = "save";
            }

            if (p.X > this.CurrentGame.TileSize * 3 && p.X < this.CurrentGame.TileSize * 4 && p.Y < this.CurrentGame.TileSize * 8 && p.Y > this.CurrentGame.TileSize * 7)
            {
                this.HeroAction(0);
                this.NewTarget(true);
                action = "HeroAction";
            }

            if (p.X > this.CurrentGame.TileSize * 4 && p.X < this.CurrentGame.TileSize * 5 && p.Y < this.CurrentGame.TileSize * 8 && p.Y > this.CurrentGame.TileSize * 7)
            {
                this.HeroAction(1);
                this.NewTarget(true);
                action = "HeroAction";
            }

            if (p.X > this.CurrentGame.TileSize * 5 && p.X < this.CurrentGame.TileSize * 6 && p.Y < this.CurrentGame.TileSize * 8 && p.Y > this.CurrentGame.TileSize * 7)
            {
                this.HeroAction(2);
                this.NewTarget(true);
                action = "HeroAction";
            }

            if (p.X > this.CurrentGame.TileSize * 3 && p.X < this.CurrentGame.TileSize * 4 && p.Y < this.CurrentGame.TileSize * 2 && p.Y > this.CurrentGame.TileSize * 1)
            {
                this.NewTarget(true, 0);
                action = "ChangeTarget";
            }

            if (p.X > this.CurrentGame.TileSize * 4 && p.X < this.CurrentGame.TileSize * 5 && p.Y < this.CurrentGame.TileSize * 2 && p.Y > this.CurrentGame.TileSize * 1)
            {
                this.NewTarget(true, 1);
                action = "ChangeTarget";
            }

            if (p.X > this.CurrentGame.TileSize * 5 && p.X < this.CurrentGame.TileSize * 6 && p.Y < this.CurrentGame.TileSize * 2 && p.Y > this.CurrentGame.TileSize * 1)
            {
                this.NewTarget(true, 2);
                action = "ChangeTarget";
            }

            if (p.X < this.CurrentGame.GameWidth - this.CurrentGame.TileSize && p.X > this.CurrentGame.TileSize && p.Y > this.CurrentGame.TileSize * 2 && p.Y < this.CurrentGame.TileSize * 7)
            {
                int posX = (int)((p.X / this.CurrentGame.TileSize) - 1);
                int posY = (int)((p.Y / this.CurrentGame.TileSize) - 2);
                this.SelectCrystal(posX, posY);
                this.NewTarget(true);
                action = "CrystalAction";
            }

            this.NextRound();
            return action;
        }

        /// <summary>
        /// Saves current game.
        /// </summary>
        public void Save()
        {
            this.CurrentGame.Save();
        }

        /// <summary>
        /// Left enemy tick.
        /// </summary>
        public void LeftEnemyTick()
        {
            this.EnemyAction(0);
        }

        /// <summary>
        /// boss tick.
        /// </summary>
        public void BossTick()
        {
            this.EnemyAction(1);
        }

        /// <summary>
        /// right enemy tick.
        /// </summary>
        public void RightEnemyTick()
        {
            this.EnemyAction(2);
        }

        /// <summary>
        /// Hero Tick.
        /// </summary>
        public void HeroTick()
        {
            this.CurrentGame.CurrentHeroes[0].IsUsable = true;
            this.CurrentGame.CurrentHeroes[1].IsUsable = true;
            this.CurrentGame.CurrentHeroes[2].IsUsable = true;
        }

        /// <summary>
        /// Saves final score.
        /// </summary>
        /// <param name="name">player name.</param>
        public void SaveScore(string name)
        {
            this.HighscoreRepo.SaveHighscore(new Highscore() { Name = name, Date = DateTime.Now, Score = this.CurrentGame.Points });
        }

        /// <summary>
        /// Enemy action method.
        /// </summary>
        /// <param name="id">id.</param>
        public void EnemyAction(int id)
        {
            if (!this.CurrentGame.CurrentHeroes[this.CurrentGame.CurrentEnemies[id].Target].IsAlive)
            {
                this.NewTarget(false, -1, id);
            }

            if (this.CurrentGame.CurrentEnemies[id].IsAlive)
            {
                switch (this.CurrentGame.CurrentEnemies[id].Type)
                {
                    case CharacterType.Archer:
                        this.DamageHero(this.CurrentGame.CurrentEnemies[id]);
                        break;
                    case CharacterType.Healer:
                        this.Heal(ref this.CurrentGame.CurrentEnemies[0]);
                        this.Heal(ref this.CurrentGame.CurrentEnemies[1]);
                        this.Heal(ref this.CurrentGame.CurrentEnemies[2]);
                        break;
                    case CharacterType.Fighter:
                        this.DamageHero(this.CurrentGame.CurrentEnemies[id]);
                        break;
                    case CharacterType.Mage:
                        this.MageAction(false);
                        break;
                    case CharacterType.Tank:
                        this.TankAction(id, false);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Changes target.
        /// </summary>
        /// <param name="isPlayer">type.</param>
        /// <param name="newTarget">new target.</param>
        /// <param name="id">sender.</param>
        /// <returns>new target id.</returns>
        public int NewTarget(bool isPlayer, int newTarget = -1, int id = -1)
        {
            if (isPlayer && !this.CurrentGame.CurrentEnemies[this.CurrentGame.Target].IsAlive)
            {
                this.CurrentGame.AddPoints(100);
            }

            return this.CurrentGame.ChangeTarget(isPlayer, newTarget, id);
        }

        /// <summary>
        /// Damage enemy.
        /// </summary>
        /// <param name="c">damage.</param>
        public void DamageEnemy(int c)
        {
            if (this.CurrentGame.CurrentEnemies[this.CurrentGame.Target].CurrentHp > c)
            {
                this.CurrentGame.CurrentEnemies[this.CurrentGame.Target].CurrentHp -= c;
            }
            else
            {
                this.CurrentGame.CurrentEnemies[this.CurrentGame.Target].CurrentHp = 0;
            }
        }

        /// <summary>
        /// Determines if the two selected crystal adjecent.
        /// </summary>
        /// <returns>bool.</returns>
        public bool IsNear()
        {
            bool isNear = false;
            if (Math.Abs(this.CurrentGame.FirstSelected.X - this.CurrentGame.SecondSelected.X) == 1 && this.CurrentGame.FirstSelected.Y == this.CurrentGame.SecondSelected.Y)
            {
                isNear = true;
            }
            else if (Math.Abs(this.CurrentGame.FirstSelected.Y - this.CurrentGame.SecondSelected.Y) == 1 && this.CurrentGame.FirstSelected.X == this.CurrentGame.SecondSelected.X)
            {
                isNear = true;
            }

            return isNear;
        }

        /// <summary>
        /// Continues to the next round.
        /// </summary>
        /// <returns>bool.</returns>
        public bool NextRound()
        {
            if (!this.CurrentGame.CurrentEnemies.Any(x => x.IsAlive))
            {
                this.CurrentGame.CurrentEnemies[0] = this.CurrentGame.CommonEnemies[rnd.Next(this.CurrentGame.CommonEnemies.Count)];
                this.CurrentGame.CurrentEnemies[2] = this.CurrentGame.CommonEnemies[rnd.Next(this.CurrentGame.CommonEnemies.Count)];
                this.CurrentGame.CurrentEnemies[1] = this.CurrentGame.BossEnemies[rnd.Next(this.CurrentGame.BossEnemies.Count)];

                this.CurrentGame.CurrentEnemies[0].CurrentHp = this.CurrentGame.CurrentEnemies[0].MaxHp;
                this.CurrentGame.CurrentEnemies[1].CurrentHp = this.CurrentGame.CurrentEnemies[1].MaxHp;
                this.CurrentGame.CurrentEnemies[2].CurrentHp = this.CurrentGame.CurrentEnemies[2].MaxHp;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Damage hero.
        /// </summary>
        /// <param name="attacker">attacker.</param>
        public void DamageHero(ICharacter attacker)
        {
            if (this.CurrentGame.CurrentHeroes[attacker.Target].CurrentHp > attacker.Damage)
            {
                this.CurrentGame.CurrentHeroes[attacker.Target].CurrentHp -= attacker.Damage;
            }
            else
            {
                this.CurrentGame.CurrentHeroes[attacker.Target].CurrentHp = 0;
            }
        }

        /// <summary>
        /// Swaps the two selected crystal.
        /// </summary>
        private void Swap()
        {
            if (this.IsNear())
            {
                CrystalType swap = this.CurrentGame.GameField[(int)this.CurrentGame.SecondSelected.Y, (int)this.CurrentGame.SecondSelected.X];
                this.CurrentGame.GameField[(int)this.CurrentGame.SecondSelected.Y, (int)this.CurrentGame.SecondSelected.X] = this.CurrentGame.GameField[(int)this.CurrentGame.FirstSelected.Y, (int)this.CurrentGame.FirstSelected.X];
                this.CurrentGame.GameField[(int)this.CurrentGame.FirstSelected.Y, (int)this.CurrentGame.FirstSelected.X] = swap;

                this.CheckRowAndColumn((int)this.CurrentGame.FirstSelected.X, (int)this.CurrentGame.FirstSelected.Y, this.CurrentGame.GameField[(int)this.CurrentGame.FirstSelected.Y, (int)this.CurrentGame.FirstSelected.X]);
                this.CheckRowAndColumn((int)this.CurrentGame.SecondSelected.X, (int)this.CurrentGame.SecondSelected.Y, this.CurrentGame.GameField[(int)this.CurrentGame.SecondSelected.Y, (int)this.CurrentGame.SecondSelected.X]);
            }

            this.CurrentGame.FirstSelected = new Point(-1, -1);
            this.CurrentGame.SecondSelected = new Point(-1, -1);
        }

        /// <summary>
        /// Fills the used crystals with new ones.
        /// </summary>
        /// <param name="x1">left horizontal index.</param>
        /// <param name="x2">right horizontal index.</param>
        /// <param name="y1">left vertical index.</param>
        /// <param name="y2">right vertical index.</param>
        /// <param name="originalX">original x position.</param>
        /// <param name="originalY">original y position.</param>
        /// <param name="verOrHor">vertical or horizontal combo.</param>
        private void Fill(int x1, int x2, int y1, int y2, int originalX, int originalY, bool verOrHor)
        {
            int count = verOrHor ? Math.Abs(y2 - y1) + 1 : Math.Abs(x2 - x1) + 1;
            List<CrystalType> newCrystals = new List<CrystalType>();
            switch (count)
            {
                case 5:
                    newCrystals.Add(CrystalType.DFA);
                    break;
                case 6:
                    newCrystals.Add(CrystalType.Execution);
                    break;
                case 7:
                    newCrystals.Add(CrystalType.CriticalStrike);
                    break;
                default:
                    break;
            }

            while (newCrystals.Count < count)
            {
                newCrystals.Add((CrystalType)rnd.Next(0, 4));
            }

            if (verOrHor)
            {
                for (int i = y1; i < y2 + 1; i++)
                {
                    int index = rnd.Next(newCrystals.Count);
                    this.CurrentGame.GameField[i, originalX] = newCrystals[index];
                    newCrystals.RemoveAt(index);
                }
            }
            else
            {
                for (int i = x1; i < x2 + 1; i++)
                {
                    int index = rnd.Next(newCrystals.Count);
                    this.CurrentGame.GameField[originalY, i] = newCrystals[index];
                    newCrystals.RemoveAt(index);
                }
            }
        }

        private int GetLeftIndex(int x, int y, CrystalType type)
        {
            while (x > 0 && this.CurrentGame.GameField[y, x - 1] == type)
            {
                x--;
            }

            return x;
        }

        private int GetRightIndex(int x, int y, CrystalType type)
        {
            while (x < this.CurrentGame.GameField.GetLength(1) - 1 && this.CurrentGame.GameField[y, x + 1] == type)
            {
                x++;
            }

            return x;
        }

        private int GetTopIndex(int x, int y, CrystalType type)
        {
            while (y > 0 && this.CurrentGame.GameField[y - 1, x] == type)
            {
                y--;
            }

            return y;
        }

        private int GetBottomIndex(int x, int y, CrystalType type)
        {
            while (y < this.CurrentGame.GameField.GetLength(0) - 1 && this.CurrentGame.GameField[y + 1, x] == type)
            {
                y++;
            }

            return y;
        }

        private void TankAction(int id, bool isPlayer)
        {
            if (isPlayer)
            {
                this.NewTarget(false, id, 0);
                this.NewTarget(false, id, 1);
                this.NewTarget(false, id, 2);
            }
            else
            {
                this.NewTarget(true, id);
            }
        }

        private void MageAction(bool isHero)
        {
            if (isHero)
            {
                this.CurrentGame.CurrentEnemies[0].CurrentHp -= 25;
                this.CurrentGame.CurrentEnemies[1].CurrentHp -= 25;
                this.CurrentGame.CurrentEnemies[2].CurrentHp -= 25;
            }
            else
            {
                this.CurrentGame.CurrentHeroes[0].CurrentHp -= this.CurrentGame.CurrentEnemies[1].Damage;
                this.CurrentGame.CurrentHeroes[1].CurrentHp -= this.CurrentGame.CurrentEnemies[1].Damage;
                this.CurrentGame.CurrentHeroes[2].CurrentHp -= this.CurrentGame.CurrentEnemies[1].Damage;
            }
        }

        private bool Heal(ref ICharacter character)
        {
            if (character.CurrentHp > 0)
            {
                if (character.CurrentHp + 40 > character.MaxHp)
                {
                    character.CurrentHp = character.MaxHp;
                }
                else
                {
                    character.CurrentHp += 40;
                }

                return true;
            }

            return false;
        }

        private CrystalType MaxCrystal()
        {
            int[] counts = new int[4];

            for (int i = 0; i < this.CurrentGame.GameField.GetLength(0); i++)
            {
                for (int j = 0; j < this.CurrentGame.GameField.GetLength(1); j++)
                {
                    switch (this.CurrentGame.GameField[i, j])
                    {
                        case CrystalType.Red:
                            counts[0]++;
                            break;
                        case CrystalType.Green:
                            counts[1]++;
                            break;
                        case CrystalType.Blue:
                            counts[2]++;
                            break;
                        case CrystalType.Yellow:
                            counts[3]++;
                            break;
                        default:
                            break;
                    }
                }
            }

            return (CrystalType)counts.ToList().IndexOf(counts.Max());
        }

        private void Execution(Point selected)
        {
            CrystalType type = this.MaxCrystal();

            int count = 0;
            for (int i = 0; i < this.CurrentGame.GameField.GetLength(0); i++)
            {
                for (int j = 0; j < this.CurrentGame.GameField.GetLength(1); j++)
                {
                    if (this.CurrentGame.GameField[i, j] == type)
                    {
                        this.CurrentGame.GameField[i, j] = (CrystalType)rnd.Next(0, 4);
                        count++;
                    }
                }
            }

            this.CurrentGame.GameField[(int)selected.Y, (int)selected.X] = (CrystalType)rnd.Next(0, 4);

            this.DamageEnemy(20 * count);
            if (count > 3)
            {
                int points = 50;
                for (int i = 5; i <= count; i++)
                {
                    points += 50 + ((i - 4) * 10);
                }

                this.CurrentGame.AddPoints(points);
            }
            else
            {
                this.CurrentGame.AddPoints(30);
            }
        }

        private void Shapeshifter(Point selected)
        {
            Random rnd = new Random();
            CrystalType type = (CrystalType)rnd.Next(0, 4);
            CrystalType max = this.MaxCrystal();
            for (int i = 0; i < this.CurrentGame.GameField.GetLength(0); i++)
            {
                for (int j = 0; j < this.CurrentGame.GameField.GetLength(1); j++)
                {
                    if (this.CurrentGame.GameField[i, j] == max)
                    {
                        this.CurrentGame.GameField[i, j] = type;
                    }
                }
            }
        }

        private void CriticalStrike(Point selected)
        {
            for (int i = 0; i < this.CurrentGame.GameField.GetLength(1); i++)
            {
                this.CurrentGame.GameField[(int)selected.Y, i] = (CrystalType)rnd.Next(0, 4);
            }

            this.CurrentGame.GameField[(int)selected.Y, (int)selected.X] = (CrystalType)rnd.Next(0, 4);

            this.CurrentGame.AddPoints(110);
        }

        private void DeathFromAbove(Point selected)
        {
            this.CurrentGame.GameField[(int)selected.Y, (int)selected.X] = (CrystalType)rnd.Next(0, 4);
            if (selected.Y < 4)
            {
                this.CurrentGame.GameField[(int)selected.Y + 1, (int)selected.X] = (CrystalType)rnd.Next(0, 4);
            }

            if (selected.Y > 0)
            {
                this.CurrentGame.GameField[(int)selected.Y - 1, (int)selected.X] = (CrystalType)rnd.Next(0, 4);
            }

            if (selected.X < 6)
            {
                this.CurrentGame.GameField[(int)selected.Y, (int)selected.X + 1] = (CrystalType)rnd.Next(0, 4);
            }

            if (selected.X > 0)
            {
                this.CurrentGame.GameField[(int)selected.Y, (int)selected.X - 1] = (CrystalType)rnd.Next(0, 4);
            }

            this.CurrentGame.AddPoints(60);
        }
    }
}
