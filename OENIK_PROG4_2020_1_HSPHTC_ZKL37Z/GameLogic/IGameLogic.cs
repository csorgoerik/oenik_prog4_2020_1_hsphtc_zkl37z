﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Repository;

    /// <summary>
    /// Interface for logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Gets or sets game model for current game.
        /// </summary>
        IGameModel CurrentGame { get; set; }

        /// <summary>
        /// Checks if there is any activable crystal.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="type">type.</param>
        /// <returns>bool.</returns>
        bool CheckRowAndColumn(int x, int y, CrystalType type);

        /// <summary>
        /// Execute heroes special action.
        /// </summary>
        /// <param name="character">Hero.</param>
        void HeroAction(int character);

        /// <summary>
        /// Selects one crystal.
        /// </summary>
        /// <param name="x">Crystal x coordinates.</param>
        /// <param name="y">Crystal Y coordinates.</param>
        void SelectCrystal(int x, int y);

        /// <summary>
        /// Detemines which action should be activated by click position.
        /// </summary>
        /// <param name="p">Click coordinates.</param>
        /// <returns>action.</returns>
        string ClickScreen(Point p);

        /// <summary>
        /// Continues to the next round.
        /// </summary>
        /// <returns>bool.</returns>
        bool NextRound();

        /// <summary>
        /// Determines if the two selected crystal adjecent.
        /// </summary>
        /// <returns>bool.</returns>
        bool IsNear();

        /// <summary>
        /// saves final score.
        /// </summary>
        /// <param name="name">player name.</param>
        void SaveScore(string name);

        /// <summary>
        /// Left enemy tick.
        /// </summary>
        void LeftEnemyTick();

        /// <summary>
        /// boss tick.
        /// </summary>
        void BossTick();

        /// <summary>
        /// right enemy tick.
        /// </summary>
        void RightEnemyTick();

        /// <summary>
        /// Hero Tick.
        /// </summary>
        void HeroTick();
    }
}
