﻿// <copyright file="ICharacter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Interface for Characters.
    /// </summary>
    public interface ICharacter
    {
        /// <summary>
        /// Gets or sets character name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets character type.
        /// </summary>
        CharacterType Type { get; set; }

        /// <summary>
        /// Gets or sets current health points.
        /// </summary>
        int CurrentHp { get; set; }

        /// <summary>
        /// Gets or sets maximum health points.
        /// </summary>
        int MaxHp { get; set; }

        /// <summary>
        /// Gets or sets Damage for the character.
        /// </summary>
        int Damage { get; set; }

        /// <summary>
        /// Gets or sets brush.
        /// </summary>
        Brush Image { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        int Target { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is on cooldown.
        /// </summary>
        bool IsUsable { get; set; }

        /// <summary>
        /// Gets or sets character speed.
        /// </summary>
        int Speed { get; set; }

        /// <summary>
        /// Gets a value indicating whether the character is alive .
        /// </summary>
        bool IsAlive { get; }
    }
}
