﻿// <copyright file="Highscore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for highscore data.
    /// </summary>
    public class Highscore
    {
        /// <summary>
        /// Gets or sets player name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets score.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets current date.
        /// </summary>
        public DateTime Date { get; set; }
    }
}
