﻿// <copyright file="HighscoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Highscore repository.
    /// </summary>
    public class HighscoreRepository : IHighscoreRepository
    {
        /// <summary>
        /// Gets all of the highscores.
        /// </summary>
        /// <returns>Highscore type list.</returns>
        public List<Highscore> GetAll()
        {
            if (!File.Exists("highscores.xml"))
            {
                return new List<Highscore>();
            }

            XDocument xml = XDocument.Load("highscores.xml");
            var data = from akt in xml.Descendants("Entry")
                       select akt;
            List<Highscore> scores = new List<Highscore>();
            foreach (var item in data)
            {
                Highscore score = new Highscore();
                score.Name = item.Element("Name").Value;
                score.Score = int.Parse(item.Element("Score").Value);
                string date = item.Element("Date").Value;
                string[] split = date.Split(':');
                score.Date = new DateTime(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]), int.Parse(split[3]), int.Parse(split[4]), int.Parse(split[5]));
                scores.Add(score);
            }

            return scores;
        }

        /// <summary>
        /// Saves an highscore result.
        /// </summary>
        /// <param name="score">score.</param>
        public void SaveHighscore(Highscore score)
        {
            if (!File.Exists("highscores.xml"))
            {
                File.Create("highscores.xml");
            }

            XDocument xml = XDocument.Load("highscores.xml");
            XElement newScore = new XElement(
                "Entry",
                new XElement("Name", score.Name),
                new XElement("Score", score.Score),
                new XElement("Date", $"{score.Date.Year}:{score.Date.Month}:{score.Date.Day}:{score.Date.Hour}:{score.Date.Minute}:{score.Date.Second}"));
            xml.Root.Add(newScore);
            xml.Save("highscores.xml");
        }
    }
}
