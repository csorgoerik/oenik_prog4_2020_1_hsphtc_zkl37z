﻿// <copyright file="CrystalType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    /// <summary>
    /// enum for crystal types.
    /// </summary>
    public enum CrystalType
    {
        /// <summary>
        /// Red crystal.
        /// </summary>
        Red,

        /// <summary>
        /// Green crystal.
        /// </summary>
        Green,

        /// <summary>
        /// Blue crystal.
        /// </summary>
        Blue,

        /// <summary>
        /// Yellow Crystal
        /// </summary>
        Yellow,

        /// <summary>
        /// Execution crystal.
        /// </summary>
        Execution,

        /// <summary>
        /// Death from above crystal.
        /// </summary>
        DFA,

        /// <summary>
        /// Critical Strike crystal.
        /// </summary>
        CriticalStrike,

        /// <summary>
        /// Shapeshifter crystal.
        /// </summary>
        Shapeshifter,
    }
}
