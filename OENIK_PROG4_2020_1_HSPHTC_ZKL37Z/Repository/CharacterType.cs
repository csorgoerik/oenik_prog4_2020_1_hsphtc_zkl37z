﻿// <copyright file="CharacterType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    /// <summary>
    /// enum for character types.
    /// </summary>
    public enum CharacterType
    {
        /// <summary>
        /// Archer character.
        /// </summary>
        Archer,

        /// <summary>
        /// Healer character.
        /// </summary>
        Healer,

        /// <summary>
        /// Fighter character.
        /// </summary>
        Fighter,

        /// <summary>
        /// Mage character.
        /// </summary>
        Mage,

        /// <summary>
        /// Tank character.
        /// </summary>
        Tank,
    }
}
