﻿// <copyright file="Character.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Character class.
    /// </summary>
    public class Character : ICharacter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// </summary>
        /// <param name="name">Name of the character.</param>
        /// <param name="type">Character type.</param>
        /// <param name="maxHp">Maximum health points.</param>
        /// <param name="image">Image brush.</param>
        /// <param name="damage">damage.</param>
        public Character(string name, CharacterType type, int maxHp, Brush image, int damage)
        {
            this.Name = name;
            this.Type = type;
            this.CurrentHp = maxHp;
            this.MaxHp = maxHp;
            this.Image = image;
            this.IsUsable = true;
            this.Damage = damage;
            switch (type)
            {
                case CharacterType.Archer:
                    this.Damage = 50;
                    break;
                case CharacterType.Healer:
                    this.Damage = 0;
                    break;
                case CharacterType.Fighter:
                    this.Damage = 25;
                    break;
                case CharacterType.Mage:
                    this.Damage = 20;
                    break;
                case CharacterType.Tank:
                    this.Damage = 15;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Gets or sets character name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets character type.
        /// </summary>
        public CharacterType Type { get; set; }

        /// <summary>
        /// Gets or sets current health points.
        /// </summary>
        public int CurrentHp { get; set; }

        /// <summary>
        /// Gets or sets maximum health points.
        /// </summary>
        public int MaxHp { get; set; }

        /// <summary>
        /// Gets or sets brush.
        /// </summary>
        public Brush Image { get; set; }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        public int Target { get; set; }

        /// <summary>
        /// Gets or sets character damage.
        /// </summary>
        public int Damage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is on cooldown.
        /// </summary>
        public bool IsUsable { get; set; }

        /// <summary>
        /// Gets or sets character speed.
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Gets a value indicating whether the character is alive .
        /// </summary>
        public bool IsAlive
        {
            get { return this.CurrentHp > 0; }
        }
    }
}
