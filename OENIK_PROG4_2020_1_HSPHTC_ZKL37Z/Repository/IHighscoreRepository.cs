﻿// <copyright file="IHighscoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for highscore repository.
    /// </summary>
    public interface IHighscoreRepository
    {
        /// <summary>
        /// Get all.
        /// </summary>
        /// <returns>Highscore type list.</returns>
        List<Highscore> GetAll();

        /// <summary>
        /// Saves highsore.
        /// </summary>
        /// <param name="score">score.</param>
        void SaveHighscore(Highscore score);
    }
}
