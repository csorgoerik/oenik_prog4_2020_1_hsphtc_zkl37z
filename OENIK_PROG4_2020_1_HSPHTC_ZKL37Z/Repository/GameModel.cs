﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml.Linq;

    /// <summary>
    /// Model which contains game data.
    /// </summary>
    public class GameModel : IGameModel
    {
        /// <summary>
        /// Random.
        /// </summary>
        private static Random rnd = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="h">height.</param>
        /// <param name="w">width.</param>
        public GameModel(double h, double w)
        {
            this.GameHeight = h;
            this.GameWidth = w;
            this.GameField = new CrystalType[5, 7];
            for (int i = 0; i < this.GameField.GetLength(0); i++)
            {
                for (int j = 0; j < this.GameField.GetLength(1); j++)
                {
                    this.GameField[i, j] = (CrystalType)rnd.Next(0, 4);
                }
            }

            this.CurrentHeroes = new ICharacter[3];
            this.CurrentEnemies = new ICharacter[3];

            BitmapImage desertedOrc = new BitmapImage();
            BitmapImage humanMage = new BitmapImage();
            BitmapImage hobbitFighter = new BitmapImage();
            BitmapImage elfHealer = new BitmapImage();
            BitmapImage gondorian = new BitmapImage();
            BitmapImage sauron = new BitmapImage();
            BitmapImage nasgul = new BitmapImage();
            BitmapImage orcFighter = new BitmapImage();
            BitmapImage orcArcher = new BitmapImage();
            BitmapImage orcHealer = new BitmapImage();

            this.CommonEnemies = new List<ICharacter>();
            orcArcher.BeginInit();
            orcArcher.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.OrcArcher.png");
            orcArcher.EndInit();
            this.CommonEnemies.Add(new Character("Orc Archer", CharacterType.Archer, 250, new ImageBrush(orcArcher), 140));
            orcFighter.BeginInit();
            orcFighter.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.OrcFighter.png");
            orcFighter.EndInit();
            this.CommonEnemies.Add(new Character("Orc Fighter", CharacterType.Fighter, 350, new ImageBrush(orcFighter), 100));
            orcHealer.BeginInit();
            orcHealer.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.OrcHealer.png");
            orcHealer.EndInit();
            this.CommonEnemies.Add(new Character("Orc Healer", CharacterType.Healer, 200, new ImageBrush(orcHealer), 0));

            this.BossEnemies = new List<ICharacter>();
            nasgul.BeginInit();
            nasgul.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.Nasgul.png");
            nasgul.EndInit();
            this.BossEnemies.Add(new Character("Nasgul", CharacterType.Tank, 400, new ImageBrush(nasgul), 0));
            sauron.BeginInit();
            sauron.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.Sauron.png");
            sauron.EndInit();
            this.BossEnemies.Add(new Character("Sauron", CharacterType.Mage, 350, new ImageBrush(sauron), 460));

            this.Heroes = new List<ICharacter>();

            gondorian.BeginInit();
            gondorian.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.GondorianArcher.png");
            gondorian.EndInit();
            this.Heroes.Add(new Character("Gondorian Archer", CharacterType.Archer, 250, new ImageBrush(gondorian), 180));

            elfHealer.BeginInit();
            elfHealer.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.ElfHealer.png");
            elfHealer.EndInit();
            this.Heroes.Add(new Character("Elf Healer", CharacterType.Healer, 250, new ImageBrush(elfHealer), 0));

            hobbitFighter.BeginInit();
            hobbitFighter.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.HobbitFighter.png");
            hobbitFighter.EndInit();
            this.Heroes.Add(new Character("Hobbit Fighter", CharacterType.Fighter, 300, new ImageBrush(hobbitFighter), 120));

            humanMage.BeginInit();
            humanMage.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.HumanMage.png");
            humanMage.EndInit();
            this.Heroes.Add(new Character("Human Mage", CharacterType.Mage, 200, new ImageBrush(humanMage), 100));

            desertedOrc.BeginInit();
            desertedOrc.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Images.DesertedOrc.png");
            desertedOrc.EndInit();
            this.Heroes.Add(new Character("Deserted Orc", CharacterType.Tank, 350, new ImageBrush(desertedOrc), 0));

            this.TileSize = w / 9;
            this.Points = 0;

            ICharacter left = this.CommonEnemies[rnd.Next(this.CommonEnemies.Count)];
            this.CurrentEnemies[0] = new Character(left.Name, left.Type, left.MaxHp, left.Image, left.Damage);
            this.CurrentEnemies[0].Target = 0;

            ICharacter right = this.CommonEnemies[rnd.Next(this.CommonEnemies.Count)];
            this.CurrentEnemies[2] = new Character(right.Name, right.Type, right.MaxHp, right.Image, right.Damage);
            this.CurrentEnemies[2].Target = 2;

            ICharacter boss = this.BossEnemies[rnd.Next(this.BossEnemies.Count)];
            this.CurrentEnemies[1] = new Character(boss.Name, boss.Type, boss.MaxHp, boss.Image, boss.Damage);
            this.CurrentEnemies[1].Target = 1;

            left = this.Heroes[rnd.Next(this.Heroes.Count)];
            this.CurrentHeroes[0] = new Character(left.Name, left.Type, left.MaxHp, left.Image, left.Damage);

            ICharacter center = this.Heroes[rnd.Next(this.Heroes.Count)];
            this.CurrentHeroes[1] = new Character(center.Name, center.Type, center.MaxHp, center.Image, center.Damage);

            right = this.Heroes[rnd.Next(this.Heroes.Count)];
            this.CurrentHeroes[2] = new Character(right.Name, right.Type, right.MaxHp, right.Image, right.Damage);
            this.FirstSelected = new Point(-1, -1);
            this.SecondSelected = new Point(-1, -1);

            this.Target = 0;
        }

        /// <summary>
        /// Gets or sets crystaltaype enum type array for game field.
        /// </summary>
        public CrystalType[,] GameField { get; set; }

        /// <summary>
        /// Gets or sets common enemies.
        /// </summary>
        public List<ICharacter> CommonEnemies { get; set; }

        /// <summary>
        /// Gets or sets boss enemies.
        /// </summary>
        public List<ICharacter> BossEnemies { get; set; }

        /// <summary>
        /// Gets or sets heroes.
        /// </summary>
        public List<ICharacter> Heroes { get; set; }

        /// <summary>
        /// Gets or sets first selected crystal.
        /// </summary>
        public Point FirstSelected { get; set; }

        /// <summary>
        /// Gets or sets second selected crystal.
        /// </summary>
        public Point SecondSelected { get; set; }

        /// <summary>
        /// Gets or sets highscore points.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets current Enemies.
        /// </summary>
        public ICharacter[] CurrentEnemies { get; set; }

        /// <summary>
        /// Gets or sets left hero.
        /// </summary>
        public ICharacter[] CurrentHeroes { get; set; }

        /// <summary>
        /// Gets or sets tile size.
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or sets game Height.
        /// </summary>
        public double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets game width.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets target.
        /// </summary>
        public int Target { get; set; }

        /// <summary>
        /// Gets a value indicating whether the game is finished.
        /// </summary>
        public bool IsFinished
        {
            get { return this.CurrentHeroes.Where(x => x.IsAlive).Count() == 0; }
        }

        /// <summary>
        /// Gets or sets speed.
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Loads a game.
        /// </summary>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        /// <returns>game model.</returns>
        public static GameModel Load(double w, double h)
        {
            XDocument xml = XDocument.Load("SaveGame.xml");
            GameModel gm = new GameModel(h, w);
            gm.Points = int.Parse(xml.Root.Element("Score").Value);

            ICharacter c = gm.Heroes.Where(x => x.Type == (CharacterType)int.Parse(xml.Root.Element("LeftHero").Attribute("Type").Value)).FirstOrDefault();
            c.MaxHp = int.Parse(xml.Root.Element("LeftHero").Attribute("MaxHp").Value);
            c.Target = int.Parse(xml.Root.Element("LeftHero").Attribute("Target").Value);
            gm.CurrentHeroes[0] = new Character(c.Name, c.Type, c.MaxHp, c.Image, c.Damage);
            gm.CurrentHeroes[0].CurrentHp = int.Parse(xml.Root.Element("LeftHero").Attribute("CurrentHp").Value);

            c = gm.Heroes.Where(x => x.Type == (CharacterType)int.Parse(xml.Root.Element("CenterHero").Attribute("Type").Value)).FirstOrDefault();
            c.MaxHp = int.Parse(xml.Root.Element("CenterHero").Attribute("MaxHp").Value);
            c.Target = int.Parse(xml.Root.Element("CenterHero").Attribute("Target").Value);
            gm.CurrentHeroes[1] = new Character(c.Name, c.Type, c.MaxHp, c.Image, c.Damage);
            gm.CurrentHeroes[1].CurrentHp = int.Parse(xml.Root.Element("CenterHero").Attribute("CurrentHp").Value);

            c = gm.Heroes.Where(x => x.Type == (CharacterType)int.Parse(xml.Root.Element("RightHero").Attribute("Type").Value)).FirstOrDefault();
            c.MaxHp = int.Parse(xml.Root.Element("RightHero").Attribute("MaxHp").Value);
            c.Target = int.Parse(xml.Root.Element("RightHero").Attribute("Target").Value);
            gm.CurrentHeroes[2] = new Character(c.Name, c.Type, c.MaxHp, c.Image, c.Damage);
            gm.CurrentHeroes[2].CurrentHp = int.Parse(xml.Root.Element("RightHero").Attribute("CurrentHp").Value);

            c = gm.CommonEnemies.Where(x => x.Type == (CharacterType)int.Parse(xml.Root.Element("LeftEnemy").Attribute("Type").Value)).FirstOrDefault();
            c.MaxHp = int.Parse(xml.Root.Element("LeftEnemy").Attribute("MaxHp").Value);
            gm.CurrentEnemies[0] = new Character(c.Name, c.Type, c.MaxHp, c.Image, c.Damage);
            gm.CurrentEnemies[0].CurrentHp = int.Parse(xml.Root.Element("LeftEnemy").Attribute("CurrentHp").Value);
            gm.CurrentEnemies[0].Target = int.Parse(xml.Root.Element("LeftEnemy").Attribute("Target").Value);

            c = gm.BossEnemies.Where(x => x.Type == (CharacterType)int.Parse(xml.Root.Element("CurrentBoss").Attribute("Type").Value)).FirstOrDefault();
            c.MaxHp = int.Parse(xml.Root.Element("CurrentBoss").Attribute("MaxHp").Value);
            gm.CurrentEnemies[1] = new Character(c.Name, c.Type, c.MaxHp, c.Image, c.Damage);
            gm.CurrentEnemies[1].CurrentHp = int.Parse(xml.Root.Element("CurrentBoss").Attribute("CurrentHp").Value);
            gm.CurrentEnemies[1].Target = int.Parse(xml.Root.Element("CurrentBoss").Attribute("Target").Value);

            c = gm.CommonEnemies.Where(x => x.Type == (CharacterType)int.Parse(xml.Root.Element("RightEnemy").Attribute("Type").Value)).FirstOrDefault();
            c.MaxHp = int.Parse(xml.Root.Element("RightEnemy").Attribute("MaxHp").Value);
            gm.CurrentEnemies[2] = new Character(c.Name, c.Type, c.MaxHp, c.Image, c.Damage);
            gm.CurrentEnemies[2].CurrentHp = int.Parse(xml.Root.Element("RightEnemy").Attribute("CurrentHp").Value);
            gm.CurrentEnemies[2].Target = int.Parse(xml.Root.Element("RightEnemy").Attribute("Target").Value);

            List<string> field = new List<string>();
            field.Add(xml.Root.Element("GameField").Element("First").Value);
            field.Add(xml.Root.Element("GameField").Element("Second").Value);
            field.Add(xml.Root.Element("GameField").Element("Third").Value);
            field.Add(xml.Root.Element("GameField").Element("Fourth").Value);
            field.Add(xml.Root.Element("GameField").Element("Fifth").Value);

            gm.GameField = ConvertStringToField(field);
            return gm;
        }

        /// <summary>
        /// Converts string to game field.
        /// </summary>
        /// <param name="field">field.</param>
        /// <returns>array.</returns>
        public static CrystalType[,] ConvertStringToField(List<string> field)
        {
            CrystalType[,] output = new CrystalType[5, 7];

            for (int i = 0; i < output.GetLength(0); i++)
            {
                string[] split = field[i].Split(':');
                for (int j = 0; j < output.GetLength(1); j++)
                {
                    output[i, j] = (CrystalType)int.Parse(split[j]);
                }
            }

            return output;
        }

        /// <summary>
        /// Saves game status.
        /// </summary>
        public void Save()
        {
            if (File.Exists("SaveGame.xml"))
            {
                File.Delete("SaveGame.xml");
            }

            var gameField = this.ConvertFieldToString();

            XDocument xml = new XDocument(
                new XElement(
                    "Game",
                    new XElement("Score", this.Points),
                    new XElement(
                        "LeftHero",
                        new XAttribute("Type", (int)this.CurrentHeroes[0].Type),
                        new XAttribute("CurrentHp", this.CurrentHeroes[0].CurrentHp),
                        new XAttribute("MaxHp", this.CurrentHeroes[0].MaxHp),
                        new XAttribute("Target", this.CurrentHeroes[0].Target)),
                    new XElement(
                        "CenterHero",
                        new XAttribute("Type", (int)this.CurrentHeroes[1].Type),
                        new XAttribute("CurrentHp", this.CurrentHeroes[1].CurrentHp),
                        new XAttribute("MaxHp", this.CurrentHeroes[1].MaxHp),
                        new XAttribute("Target", this.CurrentHeroes[1].Target)),
                    new XElement(
                        "RightHero",
                        new XAttribute("Type", (int)this.CurrentHeroes[2].Type),
                        new XAttribute("CurrentHp", this.CurrentHeroes[2].CurrentHp),
                        new XAttribute("MaxHp", this.CurrentHeroes[2].MaxHp),
                        new XAttribute("Target", this.CurrentHeroes[2].Target)),
                    new XElement(
                        "LeftEnemy",
                        new XAttribute("Type", (int)this.CurrentEnemies[0].Type),
                        new XAttribute("CurrentHp", this.CurrentEnemies[0].CurrentHp),
                        new XAttribute("MaxHp", this.CurrentEnemies[0].MaxHp),
                        new XAttribute("Target", this.CurrentEnemies[0].Target)),
                    new XElement(
                        "CurrentBoss",
                        new XAttribute("Type", (int)this.CurrentEnemies[1].Type),
                        new XAttribute("CurrentHp", this.CurrentEnemies[1].CurrentHp),
                        new XAttribute("MaxHp", this.CurrentEnemies[1].MaxHp),
                        new XAttribute("Target", this.CurrentEnemies[1].Target)),
                    new XElement(
                        "RightEnemy",
                        new XAttribute("Type", (int)this.CurrentEnemies[2].Type),
                        new XAttribute("CurrentHp", this.CurrentEnemies[2].CurrentHp),
                        new XAttribute("MaxHp", this.CurrentEnemies[2].MaxHp),
                        new XAttribute("Target", this.CurrentEnemies[2].Target)),
                    new XElement(
                        "GameField",
                        new XElement("First", gameField[0]),
                        new XElement("Second", gameField[1]),
                        new XElement("Third", gameField[2]),
                        new XElement("Fourth", gameField[3]),
                        new XElement("Fifth", gameField[4]))));

            xml.Save("SaveGame.xml");
        }

        /// <summary>
        /// Converrts game field to string type list.
        /// </summary>
        /// <returns>string type list.</returns>
        public List<string> ConvertFieldToString()
        {
            List<string> field = new List<string>();

            string temp = string.Empty;

            for (int i = 0; i < this.GameField.GetLength(0); i++)
            {
                for (int j = 0; j < this.GameField.GetLength(1); j++)
                {
                    if (j < this.GameField.GetLength(1) - 1)
                    {
                        temp = temp + $"{(int)this.GameField[i, j]}:";
                    }
                    else
                    {
                        temp = temp + $"{(int)this.GameField[i, j]}";
                    }
                }

                field.Add(temp);
                temp = string.Empty;
            }

            return field;
        }

        /// <summary>
        /// Changes the target.
        /// </summary>
        /// <param name="isPlayer">player.</param>
        /// <param name="newTarget">new target.</param>
        /// <param name="id">id.</param>
        /// <returns>bool.</returns>
        public int ChangeTarget(bool isPlayer, int newTarget, int id)
        {
            if (isPlayer)
            {
                if (!this.CurrentEnemies[this.Target].IsAlive)
                {
                    if (newTarget == -1)
                    {
                        if (this.CurrentEnemies[0].IsAlive)
                        {
                            this.Target = 0;
                            return 0;
                        }
                        else if (this.CurrentEnemies[2].IsAlive)
                        {
                            this.Target = 2;
                            return 2;
                        }
                        else
                        {
                            this.Target = 1;
                            return 1;
                        }
                    }
                    else
                    {
                        this.Target = newTarget;
                        return newTarget;
                    }
                }
                else
                {
                    if (newTarget != -1)
                    {
                        this.Target = newTarget;
                        return newTarget;
                    }
                    else
                    {
                        return newTarget;
                    }
                }
            }
            else
            {
                if (!this.CurrentHeroes[this.CurrentEnemies[id].Target].IsAlive)
                {
                    if (newTarget == -1)
                    {
                        if (this.CurrentHeroes[0].IsAlive)
                        {
                            this.CurrentEnemies[id].Target = 0;
                            return 0;
                        }
                        else if (this.CurrentHeroes[2].IsAlive)
                        {
                            this.CurrentEnemies[id].Target = 2;
                            return 2;
                        }
                        else
                        {
                            this.CurrentEnemies[id].Target = 1;
                            return 1;
                        }
                    }
                    else
                    {
                        this.CurrentEnemies[id].Target = newTarget;
                        return newTarget;
                    }
                }
                else
                {
                    if (newTarget == -1)
                    {
                        if (this.CurrentHeroes[0].IsAlive)
                        {
                            this.CurrentEnemies[id].Target = 0;
                            return 0;
                        }
                        else if (this.CurrentHeroes[2].IsAlive)
                        {
                            this.CurrentEnemies[id].Target = 2;
                            return 2;
                        }
                        else
                        {
                            this.CurrentEnemies[id].Target = 1;
                            return 1;
                        }
                    }
                    else
                    {
                        this.CurrentEnemies[id].Target = newTarget;
                        return newTarget;
                    }
                }
            }
        }

        /// <summary>
        /// Add points.
        /// </summary>
        /// <param name="points">p.</param>
        public void AddPoints(int points)
        {
            this.Points += points;
        }

        /// <summary>
        /// Damage enemy.
        /// </summary>
        /// <param name="c">damage.</param>
        public void DamageEnemy(int c)
        {
            if (this.CurrentEnemies[this.Target].CurrentHp > c)
            {
                this.CurrentEnemies[this.Target].CurrentHp -= c;
            }
            else
            {
                this.CurrentEnemies[this.Target].CurrentHp = 0;
            }
        }

        /// <summary>
        /// Damage hero.
        /// </summary>
        /// <param name="attacker">attacker.</param>
        public void DamageHero(ICharacter attacker)
        {
            if (this.CurrentHeroes[attacker.Target].CurrentHp > attacker.Damage)
            {
                this.CurrentHeroes[attacker.Target].CurrentHp -= attacker.Damage;
            }
            else
            {
                this.CurrentHeroes[attacker.Target].CurrentHp = 0;
            }
        }

        /// <summary>
        /// Next round.
        /// </summary>
        public void NextRound()
        {
            if (this.CurrentEnemies.All(x => !x.IsAlive))
            {
                ICharacter left = this.CommonEnemies[rnd.Next(this.CommonEnemies.Count)];
                this.CurrentEnemies[0] = new Character(left.Name, left.Type, left.MaxHp, left.Image, left.Damage);
                this.CurrentEnemies[0].Target = 0;

                ICharacter right = this.CommonEnemies[rnd.Next(this.CommonEnemies.Count)];
                this.CurrentEnemies[2] = new Character(right.Name, right.Type, right.MaxHp, right.Image, right.Damage);
                this.CurrentEnemies[2].Target = 2;

                ICharacter boss = this.BossEnemies[rnd.Next(this.BossEnemies.Count)];
                this.CurrentEnemies[1] = new Character(boss.Name, boss.Type, boss.MaxHp, boss.Image, boss.Damage);
                this.CurrentEnemies[1].Target = 1;
            }
        }
    }
}
