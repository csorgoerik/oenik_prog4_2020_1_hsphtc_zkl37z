﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for Game Model.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets crystaltaype enum type array for game field.
        /// </summary>
        CrystalType[,] GameField { get; set; }

        /// <summary>
        /// Gets or sets common enemies.
        /// </summary>
        List<ICharacter> CommonEnemies { get; set; }

        /// <summary>
        /// Gets or sets boss enemies.
        /// </summary>
        List<ICharacter> BossEnemies { get; set; }

        /// <summary>
        /// Gets or sets heroes.
        /// </summary>
        List<ICharacter> Heroes { get; set; }

        /// <summary>
        /// Gets or sets current boss.
        /// </summary>
        ICharacter[] CurrentEnemies { get; set; }

        /// <summary>
        /// Gets or sets left hero.
        /// </summary>
        ICharacter[] CurrentHeroes { get; set; }

        /// <summary>
        /// Gets or sets first selected crystal.
        /// </summary>
        Point FirstSelected { get; set; }

        /// <summary>
        /// Gets or sets second selected crystal.
        /// </summary>
        Point SecondSelected { get; set; }

        /// <summary>
        /// Gets or sets highscore points.
        /// </summary>
        int Points { get; set; }

        /// <summary>
        /// Gets or sets tile size.
        /// </summary>
        double TileSize { get; set; }

        /// <summary>
        /// Gets or sets game Height.
        /// </summary>
        double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets game width.
        /// </summary>
        double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets target.
        /// </summary>
        int Target { get; set; }

        /// <summary>
        /// Gets a value indicating whether the game is finished.
        /// </summary>
        bool IsFinished { get; }

        /// <summary>
        /// Gets or sets speed.
        /// </summary>
        int Speed { get; set; }

        /// <summary>
        /// Saves game status.
        /// </summary>
        void Save();

        /// <summary>
        /// Changes the target.
        /// </summary>
        /// <param name="isPlayer">player.</param>
        /// <param name="newTarget">new target.</param>
        /// <param name="id">id.</param>
        /// <returns>int.</returns>
        int ChangeTarget(bool isPlayer, int newTarget, int id);

        /// <summary>
        /// Add points.
        /// </summary>
        /// <param name="points">p.</param>
        void AddPoints(int points);

        /// <summary>
        /// Damage hero.
        /// </summary>
        /// <param name="attacker">attacker.</param>
        void DamageHero(ICharacter attacker);

        /// <summary>
        /// Damage enemy.
        /// </summary>
        /// <param name="c">damage.</param>
        void DamageEnemy(int c);

        /// <summary>
        /// Next round.
        /// </summary>
        void NextRound();
    }
}
