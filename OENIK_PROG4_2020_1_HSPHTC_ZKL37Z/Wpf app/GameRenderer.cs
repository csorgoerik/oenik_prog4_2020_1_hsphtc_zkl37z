﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Repository;

    /// <summary>
    /// Game renderer class.
    /// </summary>
    public class GameRenderer
    {
        private IGameModel model;
        private Point textLocation = new Point(10, 10);
        private Rect bgRect;
        private Typeface font = new Typeface("Arial");
        private FormattedText text;
        private int oldPoints = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">game model.</param>
        public GameRenderer(IGameModel model)
        {
            this.model = model;
            this.bgRect = new Rect(0, 0, this.model.GameWidth, this.model.GameHeight);
        }

        /// <summary>
        /// Builds drawing.
        /// </summary>
        /// <param name="ctx">drawing context.</param>
        public void BuildDrawing(DrawingContext ctx)
        {
            this.GetBackground(ctx);
            this.GetCrystals(ctx);
            this.GetHeroes(ctx);
            this.GetEnemies(ctx);
            this.GetExit(ctx);
            this.GetText(ctx);
            this.GetHeroHpBars(ctx);
            this.GetEnemyHpBars(ctx);
        }

        private void GetText(DrawingContext ctx)
        {
            if (this.oldPoints != this.model.Points)
            {
                this.text = new FormattedText(this.model.Points.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 16, Brushes.Magenta);
                Geometry textGeometry = this.text.BuildGeometry(this.textLocation);
                this.oldPoints = this.model.Points;
            }

            ctx.DrawText(this.text, this.textLocation);
        }

        private void GetHeroes(DrawingContext ctx)
        {
            Geometry leftHero = new RectangleGeometry(new Rect(3 * this.model.TileSize, 7 * this.model.TileSize, this.model.TileSize, this.model.TileSize));
            Geometry rightHero = new RectangleGeometry(new Rect(5 * this.model.TileSize, 7 * this.model.TileSize, this.model.TileSize, this.model.TileSize));
            Geometry centerHero = new RectangleGeometry(new Rect(4 * this.model.TileSize, 7 * this.model.TileSize, this.model.TileSize, this.model.TileSize));

            DrawingGroup heroes = new DrawingGroup();
            heroes.Children.Add(new GeometryDrawing(this.model.CurrentHeroes[0].Image, null, leftHero));
            heroes.Children.Add(new GeometryDrawing(this.model.CurrentHeroes[2].Image, null, rightHero));
            heroes.Children.Add(new GeometryDrawing(this.model.CurrentHeroes[1].Image, null, centerHero));

            ctx.DrawDrawing(heroes);
        }

        private void GetHeroHpBars(DrawingContext ctx)
        {
            Geometry leftHeroFull = new RectangleGeometry(new Rect(3 * this.model.TileSize, (8 * this.model.TileSize) - (this.model.TileSize * 0.15), this.model.TileSize, this.model.TileSize * 0.15));
            decimal leftHeroPer = (decimal)this.model.CurrentHeroes[0].CurrentHp / (decimal)this.model.CurrentHeroes[0].MaxHp * (decimal)this.model.TileSize;
            Geometry leftHeroCurrent = new RectangleGeometry(new Rect(3 * this.model.TileSize, (8 * this.model.TileSize) - (this.model.TileSize * 0.15), (double)leftHeroPer, this.model.TileSize * 0.15));

            Geometry centerHeroFull = new RectangleGeometry(new Rect(4 * this.model.TileSize, (8 * this.model.TileSize) - (this.model.TileSize * 0.15), this.model.TileSize, this.model.TileSize * 0.15));
            decimal centerHeroPer = (decimal)this.model.CurrentHeroes[1].CurrentHp / (decimal)this.model.CurrentHeroes[1].MaxHp * (decimal)this.model.TileSize;
            Geometry centerHeroCurrent = new RectangleGeometry(new Rect(4 * this.model.TileSize, (8 * this.model.TileSize) - (this.model.TileSize * 0.15), (double)centerHeroPer, this.model.TileSize * 0.15));

            Geometry rightHeroFull = new RectangleGeometry(new Rect(5 * this.model.TileSize, (8 * this.model.TileSize) - (this.model.TileSize * 0.15), this.model.TileSize, this.model.TileSize * 0.15));
            decimal rightHeroPer = (decimal)this.model.CurrentHeroes[2].CurrentHp / (decimal)this.model.CurrentHeroes[2].MaxHp * (decimal)this.model.TileSize;
            Geometry rightfHeroCurrent = new RectangleGeometry(new Rect(5 * this.model.TileSize, (8 * this.model.TileSize) - (this.model.TileSize * 0.15), (double)rightHeroPer, this.model.TileSize * 0.15));

            DrawingGroup bars = new DrawingGroup();
            bars.Children.Add(new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 2), leftHeroFull));
            bars.Children.Add(new GeometryDrawing(Brushes.Green, null, leftHeroCurrent));
            bars.Children.Add(new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 2), centerHeroFull));
            bars.Children.Add(new GeometryDrawing(Brushes.Green, null, centerHeroCurrent));
            bars.Children.Add(new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 2), rightHeroFull));
            bars.Children.Add(new GeometryDrawing(Brushes.Green, null, rightfHeroCurrent));

            ctx.DrawDrawing(bars);
        }

        private void GetEnemyHpBars(DrawingContext ctx)
        {
            Geometry leftEnemyFull = new RectangleGeometry(new Rect(3 * this.model.TileSize, (2 * this.model.TileSize) - (this.model.TileSize * 0.15), this.model.TileSize, this.model.TileSize * 0.15));
            decimal leftEnemyPer = (decimal)this.model.CurrentEnemies[0].CurrentHp / (decimal)this.model.CurrentEnemies[0].MaxHp * (decimal)this.model.TileSize;
            Geometry leftEnemyCurrent = new RectangleGeometry(new Rect(3 * this.model.TileSize, (2 * this.model.TileSize) - (this.model.TileSize * 0.15), (double)leftEnemyPer, this.model.TileSize * 0.15));

            Geometry centerBossFull = new RectangleGeometry(new Rect(4 * this.model.TileSize, (2 * this.model.TileSize) - (this.model.TileSize * 0.15), this.model.TileSize, this.model.TileSize * 0.15));
            decimal centerBossPer = (decimal)this.model.CurrentEnemies[1].CurrentHp / (decimal)this.model.CurrentEnemies[1].MaxHp * (decimal)this.model.TileSize;
            Geometry centerBossCurrent = new RectangleGeometry(new Rect(4 * this.model.TileSize, (2 * this.model.TileSize) - (this.model.TileSize * 0.15), (double)centerBossPer, this.model.TileSize * 0.15));

            Geometry rightEnemyFull = new RectangleGeometry(new Rect(5 * this.model.TileSize, (2 * this.model.TileSize) - (this.model.TileSize * 0.15), this.model.TileSize, this.model.TileSize * 0.15));
            decimal rightEnemyPer = (decimal)this.model.CurrentEnemies[2].CurrentHp / (decimal)this.model.CurrentEnemies[2].MaxHp * (decimal)this.model.TileSize;
            Geometry rightfEnemyCurrent = new RectangleGeometry(new Rect(5 * this.model.TileSize, (2 * this.model.TileSize) - (this.model.TileSize * 0.15), (double)rightEnemyPer, this.model.TileSize * 0.15));

            DrawingGroup bars = new DrawingGroup();
            bars.Children.Add(new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 2), leftEnemyFull));
            bars.Children.Add(new GeometryDrawing(Brushes.Green, null, leftEnemyCurrent));
            bars.Children.Add(new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 2), centerBossFull));
            bars.Children.Add(new GeometryDrawing(Brushes.Green, null, centerBossCurrent));
            bars.Children.Add(new GeometryDrawing(Brushes.Red, new Pen(Brushes.Black, 2), rightEnemyFull));
            bars.Children.Add(new GeometryDrawing(Brushes.Green, null, rightfEnemyCurrent));

            ctx.DrawDrawing(bars);
        }

        private void GetEnemies(DrawingContext ctx)
        {
            Geometry leftEnemy = new RectangleGeometry(new Rect(3 * this.model.TileSize, 1 * this.model.TileSize, this.model.TileSize, this.model.TileSize));
            Geometry rightEnemy = new RectangleGeometry(new Rect(5 * this.model.TileSize, 1 * this.model.TileSize, this.model.TileSize, this.model.TileSize));
            Geometry boss = new RectangleGeometry(new Rect(4 * this.model.TileSize, 1 * this.model.TileSize, this.model.TileSize, this.model.TileSize));

            DrawingGroup enemies = new DrawingGroup();
            enemies.Children.Add(new GeometryDrawing(this.model.CurrentEnemies[0].Image, null, leftEnemy));
            enemies.Children.Add(new GeometryDrawing(this.model.CurrentEnemies[2].Image, null, rightEnemy));
            enemies.Children.Add(new GeometryDrawing(this.model.CurrentEnemies[1].Image, null, boss));

            ctx.DrawDrawing(enemies);
        }

        private void GetExit(DrawingContext ctx)
        {
            Geometry g = new RectangleGeometry(new Rect(this.model.GameWidth - this.model.TileSize, 0, this.model.TileSize, this.model.TileSize));
            BitmapImage exit = new BitmapImage();
            ImageBrush exittile = new ImageBrush(exit);
            exit.BeginInit();
            exit.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Exit_Button_Main.png");
            exit.EndInit();
            ctx.DrawGeometry(exittile, null, g);
        }

        private void GetCrystals(DrawingContext ctx)
        {
            DrawingGroup crystals = new DrawingGroup();
            for (int y = 0; y < this.model.GameField.GetLength(0); y++)
            {
                for (int x = 0; x < this.model.GameField.GetLength(1); x++)
                {
                    Geometry box = new RectangleGeometry(new Rect((x + 1) * this.model.TileSize, (y + 2) * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                    crystals.Children.Add(new GeometryDrawing(this.GetCrystalBrush(this.model.GameField[y, x]), new Pen(Brushes.Green, 2), box));
                }
            }

            ctx.DrawDrawing(crystals);
        }

        private Brush GetCrystalBrush(CrystalType crystalType)
        {
            BitmapImage image = new BitmapImage();
            ImageBrush ib = new ImageBrush(image);
            switch (crystalType)
            {
                case CrystalType.Red:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Red_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.Green:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Green_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.Blue:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Blue_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.Yellow:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Yellow_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.Execution:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Darkgold_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.DFA:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Purple_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.CriticalStrike:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Silver_crystal.png");
                    image.EndInit();
                    return ib;
                case CrystalType.Shapeshifter:
                    image.BeginInit();
                    image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.White_crystal.png");
                    image.EndInit();
                    return ib;
                default:
                    return Brushes.Transparent;
            }
        }

        private void GetBackground(DrawingContext ctx)
        {
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Wpf_app.Images.Background.png");
            image.EndInit();
            ImageBrush ib = new ImageBrush(image);
            ctx.DrawRectangle(ib, null, this.bgRect);
        }
    }
}
