﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Navigation;
    using System.Windows.Threading;
    using GameLogic;
    using Repository;

    /// <summary>
    /// Custom game control.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private IGameLogic logic;
        private GameRenderer renderer;
        private DispatcherTimer mainTimer;
        private DispatcherTimer leftEnemyTimer;
        private DispatcherTimer bossEnemyTimer;
        private DispatcherTimer rightEnemyTimer;
        private DispatcherTimer heroTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        /// <param name="model">modle.</param>
        public GameControl(IGameModel model)
        {
            this.Model = model ?? throw new ArgumentNullException(nameof(model));
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Gets or sets game model.
        /// </summary>
        public IGameModel Model { get; set; }

        /// <summary>
        /// Render method.
        /// </summary>
        /// <param name="drawingContext">drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.BuildDrawing(drawingContext);
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.logic = new GameLogic(this.Model);
            this.renderer = new GameRenderer(this.Model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.MouseDown += this.Win_MouseDown;
                this.mainTimer = new DispatcherTimer();
                this.leftEnemyTimer = new DispatcherTimer();
                this.bossEnemyTimer = new DispatcherTimer();
                this.rightEnemyTimer = new DispatcherTimer();
                this.heroTimer = new DispatcherTimer();

                this.mainTimer.Interval = TimeSpan.FromMilliseconds(60000);
                this.leftEnemyTimer.Interval = TimeSpan.FromMilliseconds(1000);
                this.bossEnemyTimer.Interval = TimeSpan.FromMilliseconds(2000);
                this.rightEnemyTimer.Interval = TimeSpan.FromMilliseconds(1000);
                this.heroTimer.Interval = TimeSpan.FromMilliseconds(10000);

                this.mainTimer.Tick += this.MainTimer_Tick;
                this.heroTimer.Tick += this.HeroTimer_Tick;
                this.leftEnemyTimer.Tick += this.LeftEnemyTimer_Tick;
                this.bossEnemyTimer.Tick += this.BossEnemyTimer_Tick;
                this.rightEnemyTimer.Tick += this.RightEnemyTimer_Tick;

                this.mainTimer.Start();
                this.heroTimer.Start();
                this.leftEnemyTimer.Start();
                this.bossEnemyTimer.Start();
                this.rightEnemyTimer.Start();
            }

            this.InvalidateVisual();
        }

        private void RightEnemyTimer_Tick(object sender, EventArgs e)
        {
            this.logic.RightEnemyTick();
            this.IsItFinished();
            this.InvalidateVisual();
        }

        private void BossEnemyTimer_Tick(object sender, EventArgs e)
        {
            this.logic.BossTick();
            this.IsItFinished();
            this.InvalidateVisual();
        }

        private void LeftEnemyTimer_Tick(object sender, EventArgs e)
        {
            this.logic.LeftEnemyTick();
            this.IsItFinished();
            this.InvalidateVisual();
        }

        private void HeroTimer_Tick(object sender, EventArgs e)
        {
            this.logic.HeroTick();
            this.InvalidateVisual();
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            this.logic.CurrentGame.AddPoints(100);
            this.InvalidateVisual();
        }

        private void Win_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(this);
            this.logic.ClickScreen(pos);
            if (pos.X > this.Model.GameWidth - this.Model.TileSize && pos.Y < this.Model.TileSize)
            {
                NavigationService nav = NavigationService.GetNavigationService(this);
                nav.Navigate(new MainMenu());
            }

            this.InvalidateVisual();
        }

        private void IsItFinished()
        {
            if (this.logic.CurrentGame.IsFinished)
            {
                this.mainTimer.Stop();
                this.leftEnemyTimer.Stop();
                this.rightEnemyTimer.Stop();
                this.bossEnemyTimer.Stop();
                this.heroTimer.Stop();

                NavigationService nav = NavigationService.GetNavigationService(this);
                nav.Navigate(new EndGamePage(this.Model.Points));
            }
        }
    }
}
