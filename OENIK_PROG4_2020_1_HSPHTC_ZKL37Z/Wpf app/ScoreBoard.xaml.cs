﻿// <copyright file="ScoreBoard.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Repository;

    /// <summary>
    /// Interaction logic for ScoreBoard.xaml.
    /// </summary>
    public partial class ScoreBoard : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreBoard"/> class.
        /// </summary>
        public ScoreBoard()
        {
            this.InitializeComponent();
            this.ListboxUploader();
        }

        /// <summary>
        /// Gets all highscore entries.
        /// </summary>
        /// <returns>Highscore type list.</returns>
        public List<Highscore> GetAllListElements()
        {
            HighscoreRepository board = new HighscoreRepository();
            List<Highscore> scores = board.GetAll();
            Highscore teszt = new Highscore();
            teszt.Name = "Teszt";
            teszt.Score = 420;
            teszt.Date = DateTime.Now;
            scores.Add(teszt);
            return scores;
        }

        /// <summary>
        /// Fills listbox with highscores.
        /// </summary>
        public void ListboxUploader()
        {
            this.listBox1.BeginInit();
            List<Highscore> highScores = this.GetAllListElements();
            for (int i = 0; i < highScores.Count; i++)
            {
                this.listBox1.Items.Add(highScores[i].Name + "\t\t" + highScores[i].Score.ToString() + "\t\t" + highScores[i].Date.ToString() + "\t\t");
            }

            this.listBox1.EndInit();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());
        }
    }
}
