﻿// <copyright file="HeroSelectionPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Repository;

    /// <summary>
    /// Interaction logic for HeroSelectionPage.xaml.
    /// </summary>
    public partial class HeroSelectionPage : Page
    {
        private List<int> selected;

        /// <summary>
        /// Initializes a new instance of the <see cref="HeroSelectionPage"/> class.
        /// </summary>
        public HeroSelectionPage()
        {
            this.InitializeComponent();
            this.selected = new List<int>();
        }

        private void GondorianArcherButton_Click(object sender, RoutedEventArgs e)
        {
            this.listBox1.Items.Add("GONDORIAN ARCHER");
            if (this.selected.Count < 3 && !this.selected.Contains(0))
            {
                this.selected.Add(0);
            }
        }

        private void ElfhealerButton_Click(object sender, RoutedEventArgs e)
        {
            this.listBox1.Items.Add("Elf Healer");
            if (this.selected.Count < 3 && !this.selected.Contains(1))
            {
                this.selected.Add(1);
            }
        }

        private void HobbitButton_Click(object sender, RoutedEventArgs e)
        {
            this.listBox1.Items.Add("Hobbit Fighter");
            if (this.selected.Count < 3 && !this.selected.Contains(2))
            {
                this.selected.Add(2);
            }
        }

        private void HumanMageButton_Click(object sender, RoutedEventArgs e)
        {
            this.listBox1.Items.Add("Human Mage");
            if (this.selected.Count < 3 && !this.selected.Contains(3))
            {
                this.selected.Add(3);
            }
        }

        private void DesertedOrcButton_Click(object sender, RoutedEventArgs e)
        {
            this.listBox1.Items.Add("Deserted Orc");
            if (this.selected.Count < 3 && !this.selected.Contains(4))
            {
                this.selected.Add(4);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GameModel gm = new GameModel(this.ActualWidth, this.ActualHeight);
            if (this.comboBox.SelectedItem.ToString() == "Medium")
            {
                foreach (var item in gm.CommonEnemies)
                {
                    item.MaxHp += 100;
                    item.CurrentHp += 100;
                }

                foreach (var item in gm.BossEnemies)
                {
                    item.MaxHp += 100;
                    item.CurrentHp += 100;
                }

                foreach (var item in gm.CurrentEnemies)
                {
                    item.MaxHp += 100;
                    item.CurrentHp += 100;
                }
            }
            else if (this.comboBox.SelectedItem.ToString() == "Hard")
            {
                foreach (var item in gm.CommonEnemies)
                {
                    item.MaxHp += 200;
                    item.CurrentHp += 200;
                }

                foreach (var item in gm.BossEnemies)
                {
                    item.MaxHp += 200;
                    item.CurrentHp += 200;
                }

                foreach (var item in gm.CurrentEnemies)
                {
                    item.MaxHp += 200;
                    item.CurrentHp += 200;
                }
            }

            for (int i = 0; i < this.selected.Count; i++)
            {
                gm.CurrentHeroes[i] = gm.Heroes[this.selected[i]];
            }

            this.NavigationService.Navigate(new GamePage(gm));
        }
    }
}
