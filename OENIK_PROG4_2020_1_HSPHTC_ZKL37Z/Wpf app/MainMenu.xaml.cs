﻿// <copyright file="MainMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Repository;

    /// <summary>
    /// Interaction logic for MainMenu.xaml.
    /// </summary>
    public partial class MainMenu : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            this.InitializeComponent();
        }

        private void HighScoreButton_Click(object sender, RoutedEventArgs e)
        {
            ScoreBoard scoreBoard = new ScoreBoard();
            this.NavigationService.Navigate(scoreBoard);
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HeroSelectionPage());
        }

        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            GameModel gm = GameModel.Load(this.ActualWidth, this.ActualHeight);
            this.NavigationService.Navigate(new GamePage(gm));
        }
    }
}
