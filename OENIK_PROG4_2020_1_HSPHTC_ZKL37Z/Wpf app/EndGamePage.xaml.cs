﻿// <copyright file="EndGamePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Repository;

    /// <summary>
    /// Interaction logic for EndGamePage.xaml.
    /// </summary>
    public partial class EndGamePage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EndGamePage"/> class.
        /// </summary>
        /// <param name="points">points.</param>
        public EndGamePage(int points)
        {
            this.Points = points;
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets points.
        /// </summary>
        public int Points { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HighscoreRepository rep = new HighscoreRepository();
            if (this.name.Text == string.Empty)
            {
                rep.SaveHighscore(new Highscore() { Name = "NoName", Score = this.Points, Date = DateTime.Now });
            }
            else
            {
                rep.SaveHighscore(new Highscore() { Name = this.name.Text, Score = this.Points, Date = DateTime.Now });
            }

            this.NavigationService.Navigate(new MainMenu());
        }
    }
}
