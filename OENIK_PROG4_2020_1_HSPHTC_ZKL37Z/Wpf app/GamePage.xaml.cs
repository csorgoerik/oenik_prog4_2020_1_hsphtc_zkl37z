﻿// <copyright file="GamePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Wpf_app
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using Repository;

    /// <summary>
    /// Interaction logic for GamePage.xaml.
    /// </summary>
    public partial class GamePage : Page
    {
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamePage"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public GamePage(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.GamePage_Loaded;
            this.InitializeComponent();
        }

        private void GamePage_Loaded(object sender, RoutedEventArgs e)
        {
            this.frame.Content = new GameControl(this.model);
        }
    }
}
