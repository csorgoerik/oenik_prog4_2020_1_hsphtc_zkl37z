﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using GameLogic;
    using Moq;
    using NUnit.Framework;
    using Repository;

    /// <summary>
    /// Logic test.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IGameModel> mockModel;
        private GameLogic logic;

        /// <summary>
        /// Initializes mock repo and logic.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockModel = new Mock<IGameModel>();

            ICharacter[] heroes = new Character[3];
            heroes[0] = new Character("Lefthero", CharacterType.Mage, 200, null, 0);
            heroes[1] = new Character("Centerhero", CharacterType.Fighter, 200, null, 100);
            heroes[2] = new Character("Righthero", CharacterType.Healer, 0, null, 0);
            this.mockModel.Setup(x => x.CurrentHeroes).Returns(heroes);

            ICharacter[] enemies = new ICharacter[3];
            enemies[0] = new Character("LeftEnemy", CharacterType.Tank, 10, null, 0);
            enemies[1] = new Character("CurrentBoss", CharacterType.Archer, 200, null, 400);
            enemies[2] = new Character("RightEnemy", CharacterType.Archer, 200, null, 100);

            enemies[0].Target = 0;
            enemies[1].Target = 1;
            enemies[2].Target = 2;

            this.mockModel.Setup(x => x.Target).Returns(2);
            this.mockModel.Setup(x => x.CurrentEnemies).Returns(enemies);
            this.mockModel.Setup(x => x.Target).Returns(0);
            this.mockModel.Setup(x => x.TileSize).Returns(10);

            CrystalType[,] field = new CrystalType[5, 7];
            for (int i = 0; i < field.GetLength(1); i++)
            {
                field[0, i] = CrystalType.Blue;
            }

            for (int i = 0; i < field.GetLength(1) - 1; i++)
            {
                field[1, i] = CrystalType.Green;
            }

            field[1, field.GetLength(1) - 1] = CrystalType.Yellow;
            for (int i = 0; i < field.GetLength(1) - 2; i++)
            {
                field[2, i] = CrystalType.Red;
            }

            field[2, field.GetLength(1) - 1] = CrystalType.Yellow;
            field[2, field.GetLength(1) - 2] = CrystalType.Yellow;
            for (int i = 0; i < field.GetLength(1) - 3; i++)
            {
                field[3, i] = CrystalType.Yellow;
            }

            field[3, field.GetLength(1) - 1] = CrystalType.Red;
            field[3, field.GetLength(1) - 2] = CrystalType.Green;
            field[3, field.GetLength(1) - 3] = CrystalType.Blue;

            Random rnd = new Random();
            for (int i = 4; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    field[i, j] = (CrystalType)rnd.Next(4, 8);
                }
            }

            this.mockModel.Setup(x => x.GameField).Returns(field);
            this.mockModel.Setup(x => x.FirstSelected).Returns(new Point(0, 0));
            this.mockModel.Setup(x => x.SecondSelected).Returns(new Point(0, 1));

            this.logic = new GameLogic(this.mockModel.Object);
        }

        /// <summary>
        /// Tests CheckRowAndColumn method.
        /// </summary>
        [Test]
        public void Test_CheckRowAndColumn1()
        {
            bool result1 = this.logic.CheckRowAndColumn(0, 0, CrystalType.Blue);
            bool result2 = this.logic.CheckRowAndColumn(6, 4, CrystalType.Green);
            this.mockModel.Verify(x => x.AddPoints(It.IsAny<int>()), Times.Once);
            Assert.AreEqual(true, result1);
            Assert.AreEqual(false, result2);
        }

        /// <summary>
        /// Test HeroAction if it kills a target.
        /// </summary>
        [Test]
        public void Test_HeroAction()
        {
            string result = this.logic.ClickScreen(new Point(35, 75));
            this.mockModel.Verify(x => x.ChangeTarget(true, It.IsAny<int>(), It.IsAny<int>()));
            this.mockModel.Verify(x => x.AddPoints(It.IsAny<int>()), Times.Once);
            Assert.That(result == "HeroAction");
        }

        /// <summary>
        /// Test tanks special action.
        /// </summary>
        [Test]
        public void Test_Near()
        {
            bool check = this.logic.IsNear();

            Assert.IsTrue(check);
        }

        /// <summary>
        /// Test change target method.
        /// </summary>
        [Test]
        public void Test_ChangeTarget()
        {
            int target = this.logic.NewTarget(true);
            this.mockModel.Verify(x => x.ChangeTarget(true, It.IsAny<int>(), It.IsAny<int>()));
            Assert.That(target == 0);
        }

        /// <summary>
        /// Test NextRound method.
        /// </summary>
        [Test]
        public void Test_NextRound()
        {
            bool success = this.logic.NextRound();

            Assert.IsFalse(success);
        }
    }
}
